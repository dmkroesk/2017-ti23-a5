 TARGET	= ipac

# Application source en include includes
SRC_DIR	= ./source
INC_DIR = ./include
BUILD_DIR = ./build_files

# NutOS location (includes and libs)
NUT_INC = C:\ethernut-4.3.3/nut/include
NUT_BUILD_INC = C:\ethernut-4.3.3/build/gcc/atmega2561/lib/include
NUT_LIB_DIR = C:\ethernut-4.3.3/build/gcc/atmega2561/lib

# WinAvr includes
AVR_INC = C:/winavr/avr/include


# Compiler, assembler & linker (flags)
CC		= 	avr-gcc
CFLAGS	= -mmcu=atmega2561 -Os -Wstrict-prototypes -DNUT_CPU_FREQ=14745600 \
			-D__HARVARD_ARCH__ -DNUTOS_VERSION=433 \
			-Wa,-ahlms=$(BUILD_DIR)/$*lst
ASFLAGS = 	-mmcu=atmega2561 -I. -x assembler-with-cpp -Wa,-ahlms=$(BUILD_DIR)/$*lst,-gstabs
LDFLAGS	=	-mmcu=atmega2561 -Wl,--defsym=main=0,-Map=TIStreamer.map,--cref


# =================================================================================
# Source files
CFILES =        main.c			\
				uart0driver.c	\
				log.c			\
                led.c			\
				keyboard.c		\
				keyboardHandler.c \
				display.c		\
                vs10xx.c		\
                remcon.c		\
                watchdog.c		\
				mmc.c			\
				spidrv.c        \
                mmcdrv.c        \
                fat.c			\
				flash.c			\
				rtc.c           \
				menu/menu.c			\
				menu/Stream.c\
				menu/ScrollMenu.c \
				menu/Volume.c \
				menu/EditStream.c \
				menu/NewStream.c \
				menu/DeleteStream.c \
				menu/ChangeDate.c \
				menu/ChangeTimezone.c \
				menu/MainMenu.c \
				menu/Bass.c \
				menu/Treble.c \
				userdata.c\
				alarm/alarm.c \
				alarm/alarm_periodic.c \
				alarm/rtcnotifier.c \
				alarm/queue.c \
				alarm/alarms_menu.c \
				alarm/NewAlarm.c \
				alarm/timechecker.c \
				alarm/Alarm_MenuBase.c \
				alarm/EditDelete.c \
				menu/FirstStartup.c \
				player.c \
				flashSettings.c \
				alarmConverter.c \
				shoutcast.c \
				AudioFeedback.c \
				xml/uxmlparse.c \
				xml/uxmlstream.c \
				xml/uxmltree.c \
				menu/rssmenu.c

# Header files.
HFILES =        display.h        keyboard.h      keyboardHandler.h      \
                led.h                            \
                portio.h         remcon.h         log.h          \
                system.h                 settings.h     \
                                  inet.h     flashSettings.h     \
                platform.h       version.h        update.h       \
                           uart0driver.h    typedefs.h     \
                       vs10xx.h         audio.h        \
											 shoutcast.h\
				player.h 		AudioFeedback.h\
                watchdog.h       mmc.h             \
                flash.h          spidrv.h         command.h      \
                parse.h          mmcdrv.h         fat.h          \
                fatdrv.h         flash.h	  	rtc.h   \
                menu/menu.h 	menu/menu__internal.h \
				menu/Volume.h  \
				menu/ScrollMenu__internal.h	\
				alarm/alarm.h\
				alarm/queue.h alarm/timechecker.h Alarm/alarms_menu.h \
                menu/FirstStartup.h menu/Volume.h menu/Stream.h	 uxml.h\
				alarmConverter.h uxml.h menu/rssmenu.h  menu/EQ.h\
								


# Alle source files in de ./source dir
SRCS =	$(addprefix $(SRC_DIR)/,$(CFILES))
OBJS = 	$(addprefix $(BUILD_DIR)/,$(CFILES:.c=.o))

NUT_LIBS = $(NUT_LIB_DIR)/nutinit.o -lnutpro -lnutnet -lnutpro -lnutfs -lnutos -lnutdev -lnutarch -lnutnet -lnutcrt -lnutdev


# Alle includes (header files) in de ./header dir
INCS =	$(addprefix $(INC_DIR)/,$(HFILES))

# Linking rule. All *.o to elf file. Then convert to *.hex
$(TARGET):	$(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -L$(NUT_LIB_DIR) $(NUT_LIBS) -o $@.elf
	avr-objcopy -O ihex $@.elf $@.hex
#	hex2bin -ebin $@.hex

# Compiling the objs's. avr-gcc autocalls assembler
$(BUILD_DIR)/%o:	$(SRC_DIR)/%c
	$(CC) -c $< $(CFLAGS) -I$(INC_DIR) -I$(NUT_INC) -I$(AVR_INC) -o $@


all: $(TARGET)

debug:

	@echo $(OBJS)


.PHONY: clean
clean:
	-rm -f $(OBJS)
	-rm -f $(SRCS:.c=.lst)
	-rm -f *.hex *.elf *.map *.bin
