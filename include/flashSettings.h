#ifndef FLASHSETTINGS_H
#define FLASHSETTINGS_H

typedef struct
{
	int pageID;  // 0 is een setting page 1 is alarm
	int volume;
	int GMTSetting;
	int setting2;
} USERDATA_STRUCT;


extern void FLASHSETTINGS_SAVE_SETTINGS(void);
extern void FLASHSETTINGS_OPEN_SETTINGSPAGE(void);
extern int FLASHSETTINGS_STARTUP_CHECK(void);
extern int FLASHSETTINGS_RESETFLASH(void);

#endif
