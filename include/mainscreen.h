/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     Mainscreen
 * [TITLE]      mainscreen header file
 * [FILE]       mainscreen.h
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    API and gobal defines for mainscreen module
 * ======================================================================== */

#ifndef _Mainscreen_H
#define _Mainscreen_H

/*-------------------------------------------------------------------------*/
/* global defines MAGIC NUMBERS                                            */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
extern void DrawMainscreen(void);
#endif /* _Mainscreen_H */
/*  ����  End Of File  �������� �������������������������������������������� */





