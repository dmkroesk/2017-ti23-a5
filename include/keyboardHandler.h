//
// Created by cezan on 20-Feb-17.
//

#ifndef INC_2017_TI23_A5_KEYBOARDHANDLER_H
#define INC_2017_TI23_A5_KEYBOARDHANDLER_H

#include "menu/Volume.h"

void KbhInit(Volume*);
void createKeyThread(Volume*);
#endif //INC_2017_TI23_A5_KEYBOARDHANDLER_H
