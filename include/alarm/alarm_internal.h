//
// Created by flori on 2017-03-14.
//

#ifndef PROJECT_ALARM_INTERNAL_H
#define PROJECT_ALARM_INTERNAL_H

#include "alarm.h"

extern Menu *Alarm_MenuBase(Alarm*, Menu *deleteMenu, Menu *editMenu);
extern Menu *Alarm_NewAlarm(void);

#endif //PROJECT_ALARM_INTERNAL_H
