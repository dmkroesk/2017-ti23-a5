/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     Alarm
 * [TITLE]      Alarm header file
 * [FILE]       Alarm.h
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    API and gobal defines for Alarm module, standard functions of an alarm
 * ======================================================================== */

#ifndef ALARM_H
#define ALARM_H

/*-------------------------------------------------------------------------*/
/* global defines MAGIC NUMBERS                                            */
/*-------------------------------------------------------------------------*/
#include <time.h>
#include <stdbool.h>
#include <stdio.h>
#include "menu/menu.h"
#include <stdint.h>
#include "shoutcast.h"
#include "menu/Stream.h"

// to indicate an output parameter
// USE: foo(out int*)
#define out


#define Alarm_MAX_SNOOZE 10
// @TODO correct number
#define Alarm_SNOOZE_INTERFALL_S 100
// @TODO correct number
#define Alarm_PLAY_DURATION_S 100

/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/
typedef enum {PLAYING, IDLE, SNOOZED } alarm_state;

//struct sounddata
typedef struct Alarm{
  tm alarmTime; //time to trigger
  time_t alarmSeconds; //seconds since epoch, for evaluating
  Stream *stream; //stream to play audio from
  alarm_state state; //state the alarm is in

  int snooze_count; //times the alarm has been snoozed.

    void (*calculate_next_time)(struct Alarm*);
    void (*getName)(struct Alarm* , char *dst);
    Menu *menu;
}Alarm;

// typedef struct NoPAlarm{
//   tm alarmTime;
//   char StreamName[32];
//   char StreamIp[16];
//   int StreamPort;
// }NoPAlarm;


volatile struct newalarmflag{
    bool flag;
    tm *trigger_time;
}newalarmflag;


/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
extern void Alarm_start(void);

extern void Alarm_stop(void);
extern void Alarm_snooze(void);

extern void signal_changed_alarm(tm *time);

extern Menu* get_alarmsMenu(void);
// @TODO add weekday selection ect...
extern Alarm* Alarm_Periodic(tm alarm_time,Stream*);

#endif /* ALARM_H */
/*End of file */
