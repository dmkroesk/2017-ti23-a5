/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     queue
 * [TITLE]      queue header file
 * [FILE]       queue.h
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    API and gobal defines for queue module
 * ======================================================================== */

#ifndef Queue_H
#define Queue_H
#include "alarm.h"

/*-------------------------------------------------------------------------*/
/* global defines MAGIC NUMBERS                                            */
/*-------------------------------------------------------------------------*/

typedef struct queue{
    Alarm *alarm;
    struct queue *next;
}queue;

/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/
extern queue *queuePtr;
/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/

extern void init_queue(void);
extern void queue_enqueue(Alarm *newalarm);
extern Alarm* queue_dequeue(void);
extern void queue_remove(tm alarm);
extern int isEmpty(void);
extern void show_queue(void);

void QueuePrint(void);
void QueueTest(void);

#endif /*  Queue_H */
/*  ����  End Of File  �������� �������������������������������������������� */
