#ifndef SHOUTCAST_INC
#define SHOUTCAST_INC

#define STREAM_COUNT 3
#include "menu/Stream.h"
extern const Stream Streams[STREAM_COUNT];

int Port;
char *IP1;

int Internet;

int initInet(void);
int connectToStream(void);
int playStream(void);
int stopStream(void);
int playBeep(void);
FILE * connect_to_rss(const char *ip, const int port, const char *route);


#endif
