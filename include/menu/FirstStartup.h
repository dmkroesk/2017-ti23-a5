/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     FirstStartup
 * [TITLE]      firststartup header file
 * [FILE]       FirstStartup.h
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    API and gobal defines for firststartup module
 * ======================================================================== */

#ifndef INC_2017_TI23_A5_FIRSTSTARTUP_H
#define INC_2017_TI23_A5_FIRSTSTARTUP_H


/*-------------------------------------------------------------------------*/
/* global defines MAGIC NUMBERS                                            */
/*-------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
extern void FirstStartup_Draw(void);
extern void FirstStartup_HandleInput(int button);
extern void FirstStartup_Enter(void);

extern int GMT_TIME;
extern int cont;
#endif //INC_2017_TI23_A5_FIRSTSTARTUP_H
/*  ����  End Of File  �������� �������������������������������������������� */
