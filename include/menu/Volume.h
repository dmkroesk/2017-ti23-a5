#ifndef _Volume_H
#define _Volume_H

#include "menu/menu.h"

typedef struct
{
    int increment;
    int volume;
    int percentage;

    Menu *prev;
}Volume;

#endif
