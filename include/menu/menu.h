/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     Menu
 * [TITLE]      Menu header file
 * [FILE]       menu.h
 * [VSN]        1.0
 * [PURPOSE]    API and gobal defines for menu module
 * ======================================================================== */
#ifndef _Menu_H
#define _Menu_H
/*-------------------------------------------------------------------------*/
/* global defines                                                          */
/*-------------------------------------------------------------------------*/
#include <stdbool.h>
#include "time.h"
#include "menu/EQ.h"

/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/
typedef struct Menu {
        char *name;

        void *data;

        void (*handle_input)(struct  Menu *,int input);
        void (*on_enter)(struct Menu *,struct Menu*);
        void (*on_return)(struct Menu *);
        void (*draw)(struct Menu *);
}Menu;

extern Menu *main_menu;

extern void Menu_init(Menu *);
extern void Menu_handle_input(int input);
extern void Menu_draw(bool force);

extern Menu* Menu_ScrollMenu(char *name, const Menu *const, int length);
extern Menu* Menu_ScrollMenu_InstaReturn(char *name, Menu *, int length);
extern Menu* TempMenu_ScrollMenu(char *name, int length);
extern Menu* TempMenu_stream(char *name, char *ip, int port);
extern Menu* Menu_Volume(void);
extern Menu* Menu_EditStream(void);
extern Menu* Menu_NewStream(void);
extern Menu* Menu_DeleteStream(void);
extern Menu* Menu_ChangeDate(void);
extern Menu* Menu_ChangeTimezone(void);
extern Menu *Menu_Bass(EQ*);
extern Menu *Menu_Treble(EQ*);
extern Menu* Alarm_EditDelete(tm tm);
extern Menu* Alarm_NewAlarmWithTM(tm tm);
extern Menu* Menu_MainMenu(Menu *);
extern Menu* Menu_Rss(void);
#endif
