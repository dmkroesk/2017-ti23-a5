//
// Created by flori on 2017-03-14.
//

#ifndef PROJECT_SCROLLMENU_INTERNAL_H_H
#define PROJECT_SCROLLMENU_INTERNAL_H_H

typedef struct
{
    Menu *menus; /// the menus we are scrolling through
    int length; /// the length of the menus array
    int index; /// our current index in the array

    Menu *prev; /// the previous menu, for when we need to return
}ScrollMenu;

#endif //PROJECT_SCROLLMENU_INTERNAL_H_H
