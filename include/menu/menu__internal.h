//
// Created by flori on 2/21/2017.
//

#ifndef PROJECT_MENU_INTERNAL_H
#define PROJECT_MENU_INTERNAL_H

#include "menu.h"

#define ESC             7
#define UP              8
#define OK              9
#define LEFT           10
#define DOWN           11
#define RIGHT          12
#define ESCOK          14

#define MAX_MENU_SIZE   7
#define MAX_NAME_SIZE  10

extern void Menu_set(Menu *);

#endif //PROJECT_MENU_INTERNAL_H
