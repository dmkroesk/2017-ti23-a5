/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     rssmenu
 * [TITLE]      rssmenu header file
 * [FILE]       rssmenu.h
 * [VSN]        1.0
 * [PURPOSE]    API and gobal defines for rssmenu module
 * ======================================================================== */

#ifndef _RSS_MENU_H
#define _RSS_MENU_H

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>

#include "shoutcast.h"
#include "uxml.h"
#include "menu/menu__internal.h"
#include "display.h"

#define MAX_HEADLINES 2
#define MAX_CHAR_HEADLINE 8
/*-------------------------------------------------------------------------*/
/* global defines MAGIC NUMBERS                                            */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/
typedef struct rss{
  char headlines[MAX_HEADLINES][MAX_CHAR_HEADLINE]; //2 headlines with each max 8 characters.
  char* title;
  Menu* prev;
}rss;

/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
#endif /* _RSS_MENU_H */
/*  ����  End Of File  �������� �������������������������������������������� */
