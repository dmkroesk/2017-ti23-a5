#ifndef _EQ_H
#define _EQ_H

#include "menu/menu.h"

typedef struct
{
    int bincrement;
    int bass;

    int tincrement;
    int treble;

    struct Menu *prev;
}EQ;

#endif
