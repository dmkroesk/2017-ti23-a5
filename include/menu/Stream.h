#ifndef _Stream_H
#define _Stream_H
#include "menu/menu.h"

typedef struct
{
    char* name;
    char* ip;
    int port;

    Menu *prev;
}Stream;

#endif
