#ifndef USERDATA_INC
#define USERDATA_INC

#include "alarm/Alarm.h"

// typedef struct
// {
// 	int pageID;  // 0 is een setting page 1 is alarm
// 	int volume;
// 	int setting1;
// 	int setting2;
// } USERDATA_STRUCT;

typedef struct{
	char pageID[12];
	char name[16];
  int hours;
	int minutes;
} ALARM_STRUCT;



int initUserData(void);
int saveAlarm(ALARM_STRUCT*src, int size);
//int savePersistent(USERDATA_STRUCT *src, int size, int page);
//int savePersistentAlarm(NoPAlarm *src, int size, int page);
//int openPersistent(USERDATA_STRUCT *src, int size, int page);
//int openPersistentAlarm(NoPAlarm *src, int size, int page);
void showPage(u_long pgn);

void testRomFs(void);

#endif
