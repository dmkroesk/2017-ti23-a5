#ifndef ALARMCONVERTER_H
#define ALARMCONVERTER_H

#include "alarm/alarm.h"
#include "menu/Stream.h"

typedef struct
{
	unsigned int pageID;  // 0 is een setting page 1 is alarm
	unsigned char StreamName[20];
  unsigned char StreamIp[15];
  unsigned int alarmHour;
  unsigned int alarmMin;
  unsigned int StreamPort;

}NOPALARM;


extern int ALARMCONVERTER_saveAlarm(Alarm alarm);
extern int ALARMCONVERTER_ResetAlarm(void);
extern int ALARMCONVERTER_LoadAlarms(void);
extern int ALARMCONVERTER_RemoveAlarm(tm alarmTime);
extern int showAlarm(NOPALARM *salarmNoP);

#endif
