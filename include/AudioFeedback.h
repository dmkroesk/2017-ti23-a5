#ifndef AUDIOFEEDBACK_H
#define AUDIOFEEDBACK_H

extern int AF_StartupBeep(void);
extern int AF_VolumeBeep(void);
extern int AF_MaxVolume(void);
extern int AF_ButtonBeep(void);
#endif
