/*! \mainpage SIR firmware documentation
 *
 *  \section intro Introduction
 *  A collection of HTML-files has been generated using the documentation in the sourcefiles to
 *  allow the developer to browse through the technical documentation of this project.
 *  \par
 *  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
 *  documentation should be done via the sourcefiles.
 */

/*! \file
 *  COPYRIGHT (C) STREAMIT BV 2010
 *  \date 19 december 2003
 */



#define LOG_MODULE    LOG_MAIN_MODULE

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include "C:\ethernut-4.3.3\nutbld\include\cfg\os.h"


#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "alarm/queue.h"

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "shoutcast.h"
#include "player.h"
#include "vs10xx.h"
#include <stdint.h>
#include <inttypes.h>

#include <time.h>
#include "rtc.h"
#include "alarm/queue.h"
#include "alarm/alarm.h"
#include "menu/menu.h"


#include "userdata.h"
#include "time.h"
#include "flashSettings.h"
#include "menu/Volume.h"



#include "menu/FirstStartup.h"
#include "menu/Stream.h"
#include "mmcdrv.h"
#include "AudioFeedback.h"

/*-------------------------------------------------------------------------*/
/* global variable definitions                                             */
/*-------------------------------------------------------------------------*/
int  cont;
int  GMT_TIME;
Menu *main_menu;
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/


/* Some dummy user data for testing the persistent data storage */
// static USERDATA_STRUCT userStruct[] = {
//    {
//     "UserPage",
//       254,
//       4,
//     5,
//    }
// };

static void SysMainBeatInterrupt(void *);
static void SysControlMainBeat(u_char);
static void FlashSettings(void);

static void showUserData(USERDATA_STRUCT *userStruct, int len);


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/
void __attribute__ ((noinline)) StackTest(int depth)
{
	printf("depth=%03i ptr=%p\n", depth, &depth);
	StackTest(++depth);
}


/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */

/*!
 * \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
 *
 * This routine is automatically called during system
 * initialization.
 *
 * resolution of this Timer ISR is 4,448 msecs
 *
 * \param *p not used (might be used to pass parms from the ISR)
 */
/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */
static void SysMainBeatInterrupt(void *p)
{
	/*
	 *  scan for valid keys AND check if a MMCard is inserted or removed
	 */
	KbScan();
	CardCheckCard();
}


/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */

/*!
 * \brief Initialise Digital IO
 *  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
 *
 *  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
 *  is written to the pin (PORTxn='1')
 */
/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */
void SysInitIO(void)
{
	/*
	 *  Port B:     VS1011, MMC CS/WP, SPI
	 *  output:     all, except b3 (SPI Master In)
	 *  input:      SPI Master In
	 *  pull-up:    none
	 */
	outp(0xF7, DDRB);

	/*
	 *  Port C:     Address bus
	 */

	/*
	 *  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
	 *  output:     Keyboard colums 2 & 3
	 *  input:      LCD_data, SDA, SCL (TWI)
	 *  pull-up:    LCD_data, SDA & SCL
	 */
	outp(0x0C, DDRD);
	outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

	/*
	 *  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
	 *  output:     CS Flash, LCD BL/Enable, USB Tx
	 *  input:      VS1011 (DREQ), RTL8019, IR
	 *  pull-up:    USB Rx
	 */
	outp(0x8E, DDRE);
	outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

	/*
	 *  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
	 *  output:     LCD RS/RW, LED
	 *  input:      Keyboard_Rows, MCC-detect
	 *  pull-up:    Keyboard_Rows, MCC-detect
	 *  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
	 */
#ifndef USE_JTAG
	sbi(JTAG_REG, JTD);          // disable JTAG interface to be able to use all key-rows
	sbi(JTAG_REG, JTD);          // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256
#endif //USE_JTAG

	outp(0x0E, DDRF);
	outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

	/*
	 *  Port G:     Keyboard_cols, Bus_control
	 *  output:     Keyboard_cols
	 *  input:      Bus Control (internal control)
	 *  pull-up:    none
	 */
	outp(0x18, DDRG);
}


/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */

/*!
 * \brief Starts or stops the 4.44 msec mainbeat of the system
 * \param OnOff indicates if the mainbeat needs to start or to stop
 */
/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */
static void SysControlMainBeat(u_char OnOff)
{
	int nError = 0;

	if (OnOff == ON)
	{
		nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
		if (nError == 0)
		{
			init_8_bit_timer();
		}
	}
	else
	{
		// disable overflow interrupt
		disable_8_bit_timer_ovfl_int();
	}
}


/**
 * Flashes the settings on the screen
 * Shows the GMT setting from Flash memory.
 * @TODO get GMT setting from flash
 */
void FlashSettings()
{
	LcdClear();
	int  GMT_TIME;
	char stringTimezone;          // convert GMT(int) to string
	itoa(GMT_TIME, &stringTimezone, 10);
	SetCursor(0, TOP);
	LcdString("GMT setting: ", 13);

	// Logic for drawing negative and positive GMT settings
	SetCursor(7, BOTTOM);
	if (GMT_TIME > 0)
	{
		LcdChar('+');
	}
	else if (GMT_TIME < 0)
	{
		// do nothing, negative value will be automaticly prepended with a minus sign
	}
	else
	{
		LcdChar(' ');
	}

	// Logic for drawing GMT based on value (no appending chars)
	if ((GMT_TIME < 10) && (GMT_TIME > -1))
	{
		LcdString(&stringTimezone, 1);
		LcdChar(' ');
	}
	else if (GMT_TIME < -9)
	{
		LcdString(&stringTimezone, 3);
	}
	else
	{
		LcdString(&stringTimezone, 2);
		LcdChar(' ');
	}
}


/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */

/*!
 * \brief Main entry of the SIR firmware
 *
 * All the initialisations before entering the for(;;) loop are done BEFORE
 * the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
 * initialisatons need to be done again when leaving the Setup because new values
 * might be current now
 *
 * \return \b never returns */
/* ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ */


int main(void)
{
	int t = 0;

	tm gmt;

	WatchDogDisable();
	NutDelay(100);
	SysInitIO();

	SPIinit();

	LedInit();

	LcdLowLevelInit();

	Uart0DriverInit();
	Uart0DriverStart();
	LogInit();
	LogMsg_P(LOG_INFO, PSTR("Hello World"));

	CardInit();

	X12Init();
	if (X12RtcGetClock(&gmt) == 0)
	{
		LogMsg_P(LOG_INFO, PSTR("RTC time [%02d:%02d:%02d]"), gmt.tm_hour, gmt.tm_min, gmt.tm_sec);
	}
	/* Initialize by reading the dataflash chip to check for its type */

	if (At45dbInit() == AT45DB041B)
	{
		LogMsg_P(LOG_INFO, PSTR("User dataflash is AT45DB041B"));
	}
	else
	{
		LogMsg_P(LOG_ERR, PSTR("Unexpected dataflash type"));
	}

	RcInit();
	init_queue();
	KbInit();

	//FLASHSETTINGS_RESETFLASH();
	//  FLASHSETTINGS_STARTUP_CHECK();

	SysControlMainBeat(ON);                          // enable 4.4 msecs hartbeat interrupt




    Menu *audio_content = malloc(sizeof(Menu)*3);
    audio_content[0] = *Menu_Volume();
    EQ *data = malloc(sizeof(EQ));
    audio_content[1] = *Menu_Bass(data);
    audio_content[2] = *Menu_Treble(data);
		KbhInit(audio_content[0].data);
		FLASHSETTINGS_INIT(audio_content[0].data);
			NutSleep(100);




    /* Enable global interrupts */
    sei();

	/*
	 * Increase our priority so we can feed the watchdog.
	 */
	NutThreadSetPriority(1);


    //DONT DELETE THIS This is needed to let the KB thread startup before being able to recoginze key inputs
    NutSleep(100);
	/* Show what the data flash page for the user data currently contains */         // Attention, hardcoded page number
	printf("%i\n", KbGetKey());
	if ((KbGetKey() == 6) || (FLASHSETTINGS_STARTUP_CHECK() == 1))
	{
		int FlashTemp = FLASHSETTINGS_RESETFLASH();
		printf("%s\n", "A first Start up %d", FlashTemp);
		FirstStartup_Start();
    ALARMCONVERTER_ResetAlarm();
		//FLASHSETTINGS_SAVE_SETTINGS();

		while (cont)
		{
			NutSleep(10);
		}
	}
	else
	{
		//Load from flash
		FLASHSETTINGS_OPEN_SETTINGSPAGE();

		//Show Settings FlashSettings();
	}


   ALARMCONVERTER_LoadAlarms();



    AF_StartupBeep();
    //init_queue(); //init queue, wont work without initializing!
    // @TODO @Kevin fix this crash
    //rtcnotifier_init();

    // Alarm* testAlarm;
    // NoPAlarm* testSaveAlarm;
		//
    // tm time;
    init_queue(); //init queue, wont work without initializing!
    rtcnotifier_init();
    /* Init network adapter */
    LogMsg_P(LOG_INFO, PSTR("%s"), "trying internet, takes max 30 sec" );
   if(initInet()){
       getSntpTime();
   }
   UpdateSNTPInit();

	 LogMsg_P(LOG_INFO, PSTR("%s"), "Done trying");

	LcdClear();
	LcdBackLight(LCD_BACKLIGHT_ON);

	char date[10];
		//DONT DELETE THIS This is needed to let the KB thread startup before being able to recoginze key inputs
	Menu *main_scroll;
	{
		Menu *content = malloc(sizeof(Menu) * 3);
		{                  // Audio
			content[0] = *Menu_ScrollMenu("Audio", audio_content, 3);
		}
		{                  // Streams
			                // Temp Menu for selecting between 3 streams
			                //content[1] = *TempMenu_ScrollMenu("Streams", 3);

			//Menu for later which is defined in the flow chart
			// Menu *streams_content = malloc(sizeof(Menu)*3);
			// streams_content[0] = *Menu_NewStream();
			// streams_content[1] = *Menu_EditStream();
			// streams_content[2] = *Menu_DeleteStream();
			// content[1] = *Menu_ScrollMenu("Streams", streams_content, 3);
		}
		{                  // Date & Time
			// Menu *date_content = malloc(sizeof(Menu) * 2);
			// date_content[0] = *Menu_ChangeDate();
			// date_content[1] = *Menu_ChangeTimezone();
			// content[1]      = *Menu_ScrollMenu("Date / Time", date_content, 2);
		}
		{                 //alarms
			content[1] = *get_alarmsMenu();
		}
		{
			content[2]  = *Menu_Rss();
			main_scroll = Menu_ScrollMenu_InstaReturn("main", content, 3);
		}
	}
	main_menu = Menu_MainMenu(main_scroll);





    LcdClear();
    LcdBackLight(LCD_BACKLIGHT_ON);

    Menu_init(main_menu);
    Menu_draw(true);
    for (;;)
    {
        NutSleep(1000);

				Volume* vol = (audio_content[0].data);
				VsSetVolume(vol->volume,vol->volume);

        Menu_draw(false);
        if(aborted == 1){
          VsPlayerStop();
          stopStream();
        }
        // Show time on display
        //DrawMainscreen();
        WatchDogRestart();
    }
	//stopStream();

	return(0);

	// never reached, but 'main()' returns a non-void, so.....
}
