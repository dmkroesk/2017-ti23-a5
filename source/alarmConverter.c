
#include <sys/thread.h>
#include <sys/timer.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "system.h"
#include "flash.h"
//#include "userdata.h"

#include <fs/fs.h>
#include <dev/urom.h>
#include <fs/uromfs.h>
#include <io.h>
#include "alarm/Alarm.h"
#include "alarmConverter.h"

#include "userdata.h"
#include <string.h>
#include <menu/Volume.h>
#include <menu/FirstStartup.h>
#include <menu/stream.h>
#define NOK -1

static NOPALARM alarmNoP[] = {{}};
static NOPALARM alarmNopTemp[] = {{}};


//given functions by nutos no credits for me
int savePersistentAlarm(NOPALARM *src, int size, int page)
{
    int result = NOK;

    unsigned char *storage = (unsigned char *) malloc(sizeof(unsigned char) * size);
    if( storage != NULL )
    {
        memcpy( (unsigned char *)storage, src, size );
        At45dbPageWrite(page, (unsigned char *)storage, size);
        result = 0;
    }
    free(storage);
    return result;
}


//given functions by nutos no credits for me
int openPersistentAlarm(NOPALARM *src, int size, int page)
{
    int result = NOK;

    unsigned char *storage = (unsigned char *) malloc(sizeof(unsigned char) * size);
    if( storage != NULL )
    {
        At45dbPageRead(page, (unsigned char *)storage, size);
        memcpy((NOPALARM* )src, (unsigned char *)storage, size);

        result = 0;
    }
    free(storage);
    return result;
}






// converts the alarm from a pointer struct to a non pointer struct so it can be stored
int ALARMCONVERTER_saveAlarm(Alarm alarm)
{
  /*
  debug prints
  **********************************************************************************************************************
  */
    printf("\nalarm stream name:  %s\nalarm tijd: %d\n", alarm.stream->name, alarm.alarmTime.tm_hour);

    int i = 0;
    alarmNoP[0].pageID = 1234;
    alarmNoP[0].alarmHour = alarm.alarmTime.tm_hour;
    printf("%d alarms hour\n", alarmNoP[0].alarmHour );
    alarmNoP[0].alarmMin = alarm.alarmTime.tm_min;
    while(1)
    {
      if(alarm.stream->name[i] != '\0')
      {
      alarmNoP[0].StreamName[i] = alarm.stream->name[i];
      }
      else{
        i = 0;
        break;
      }
      i +=1;
    }
    while(1)
    {
      if(alarm.stream->ip[i] != '\0')
      {
        alarmNoP[0].StreamIp[i] = alarm.stream->ip[i];
      }
      else{
        i = 0;
        break;
      }
      i +=1;
    }
    alarmNoP[0].StreamPort = alarm.stream->port;
    /*
    debug prints
    **********************************************************************************************************************
    printf("\nNoPalarm stream name: %s  ip is  %s   port is : %d hours zijn: %d  \n", alarmNoP[0].StreamName ,alarmNoP[0].StreamIp, alarmNoP[0].StreamPort, alarmNoP[0].alarmHour);
*/

    WriteAlarmNoP(&alarmNoP[0]);
    showAlarm(&alarmNoP[0]);
  //  openAlarmNop(&test[0]);
    return 0;
}

ALARMCONVERTER_RemoveAlarm(tm alarmTime)
{
  int alarmNr = NumberOfAlarms();
  int i;
  int deleteSucces = -1;
  printf("alarm hour is:  %d alarm min is: %d\n", alarmTime.tm_hour, alarmTime.tm_min);
  for(i = 7; i < 8 + NumberOfAlarms(); i++)
  {
    openPersistentAlarm((NOPALARM*)alarmNopTemp, sizeof(alarmNopTemp[0]), i);
    printf("alarm from flash hour: %d  alarm from flash min: %d\n",alarmNopTemp[0].alarmHour, alarmNopTemp[0].alarmMin );
    if(alarmNopTemp[0].alarmHour == alarmTime.tm_hour && alarmNopTemp[0].alarmMin == alarmTime.tm_min)
    {
      printf("alarm found op pagina %d \n", i);
      alarmNoP[0].pageID = 0000;
      alarmNoP[0].alarmHour = 0;
      alarmNoP[0].alarmMin = 0;
      alarmNoP[0].StreamName[0] = ' ';
      alarmNoP[0].StreamIp[0] = ' ';
      alarmNoP[0].StreamPort = 0;
      while(1)
      {
        deleteSucces = savePersistentAlarm((NOPALARM*)alarmNoP, sizeof(alarmNoP[0]), i);
        if(deleteSucces == 0)
        {
          printf("deleteSucces  -1 is fail 0 is succes? %d\n", deleteSucces );
          break;
        }
      }
    }
    else
    {
      printf("alarm not found" );
    }
  }
}
//returns 1 when reset is complete and resets all alarms from the flash memmory
int ALARMCONVERTER_ResetAlarm(void)
{
    //check number of alarms
    int alarms = NumberOfAlarms();
    int i;
    int page = 7;
    printf("number of alarms is %d\n", alarms );
    for(i = 0; i < alarms+1; i++)
    {
      int i = 0;
      alarmNoP[0].pageID = 0000;
      alarmNoP[0].alarmHour = 0;
      alarmNoP[0].alarmMin = 0;
      alarmNoP[0].StreamName[0] = ' ';
      alarmNoP[0].StreamIp[0] = ' ';
      alarmNoP[0].StreamPort = 0;
      WriteAlarmNoP(&alarmNoP[0]);
  if(0 == savePersistentAlarm((NOPALARM*)alarmNoP, sizeof(alarmNoP[0]), page+ i))
  {
    printf("\nreset 1 \n");
    NutSleep(100);
    return 1;
  }
  else{
    printf("commit sudoku\n" );
    showPage(0x08);
    return 0;
    }
  }
}

int NumberOfAlarms(void)
{
  int page = 6;
  int misses = 0;
  int alarms = 0;

  while(1)
  {
    openPersistentAlarm((NOPALARM*)alarmNopTemp, sizeof(alarmNopTemp[0]), page);
    if(alarmNopTemp[0].pageID == 1234)
      alarms++;
    else
      misses++;
    page++;
    if(misses == 5)
      break;
  }
  printf("number of alarms is: %d\n", alarms);
  return alarms;
}

int WriteAlarmNoP(NOPALARM *alarmNoP)
{
  int alarms = NumberOfAlarms();

  /*
  debug prints
  **********************************************************************************************************************
  */
  printf("page is: %d", 7 +alarms);
  printf("page id is %d\n", alarmNoP[0].pageID );
  printf("in saveAlarmNoP naam: %s  stream ip is: %s \n", alarmNoP[0].StreamName, alarmNoP[0].StreamIp);
  printf("in saveAlarmNoP uren zijn %d minuten zijn %d\n", alarmNoP[0].alarmHour, alarmNoP[0].alarmMin );


  if(0 == savePersistentAlarm((NOPALARM*)alarmNoP, sizeof(alarmNoP[0]), (7 + alarms) ))
  {
    printf("\nsaving alarm \n");
    NutSleep(100);
  //  showAlarm(alarmNoP);
  }
  else{
    printf("save failed\n" );
    showPage((7+ alarms));
  }
  return 0;
}
//loads the alarms from flash and adds it to the system memory
int ALARMCONVERTER_LoadAlarms(void)
{
    int page = 6;
    int misses = 0;
    while(1)
      {
        openPersistentAlarm((NOPALARM *)alarmNoP, sizeof(alarmNoP[0]), page);
        if(alarmNoP[0].pageID == 1234)
        {

          printf("alarm found on page %d \n", page);
          printf("alarm hours is %d\n", alarmNoP[0].alarmHour);
          printf("alarm min is %d \n", alarmNoP[0].alarmMin);

          showPage(page);
          if(alarmNoP[0].alarmHour < 24 && alarmNoP[0].alarmHour >= 0 && alarmNoP[0].alarmMin < 59 && alarmNoP[0].alarmMin >=0)
          {
            showPage(page);
            showAlarm(alarmNoP);
            Alarm *tempalarm;
             tm alarm_time;
             Stream* alarm_stream;
             alarm_time.tm_hour = alarmNoP[0].alarmHour;
             alarm_time.tm_min = alarmNoP[0].alarmMin;
             alarm_stream->name = alarmNoP[0].StreamName;
             alarm_stream->ip = alarmNoP[0].StreamIp;
             alarm_stream->port = alarmNoP[0].StreamPort;
             tempalarm = Alarm_Periodic(alarm_time, alarm_stream);
              queue_enqueue(tempalarm);
          }
          else
          {
            //todo bad alarms handle (no time)
          }

        }
        else
          misses++;
        if(misses == 4)
          break;
        page++;
    }
}
// this function is not working yet it is an idea to cleanup your flash memory
int ALARMCONVERTER_CleanFlash(void)
{
  int page = 6;
  int misses = 0;
  while(1)
    {
      openPersistentAlarm((NOPALARM *)alarmNoP, sizeof(alarmNoP[0]), page);
      if(alarmNoP[0].pageID == 1234)
      {
        if(alarmNoP[0].alarmHour > 24 || alarmNoP[0].alarmHour < 0 || alarmNoP[0].alarmMin > 60 || alarmNoP[0].alarmMin< 0)
        {
          alarmNoP[0].pageID = 0000;
          WriteAlarmNoP(alarmNoP);
          printf("a alarm has been cleaned \n");
        }
      }
      else
        misses++;
      if(misses == 4)
        break;
      page++;
  }
  return 0;
}

// prints the given alarm usualy used for debug.
int showAlarm(NOPALARM *alarmNoP)
{
  int i = 0;
    printf("alarm.pageID %d \n", alarmNoP[0].pageID );
    printf("alarm.alarmTime.tm_hour = %d\n\n", alarmNoP[0].alarmHour);
		printf("alarm.alarmTime.tm_min = %d\n\n", alarmNoP[0].alarmMin);
		printf("alarm.stream = %s\n\n", alarmNoP[0].StreamName);
    return 0;
}
