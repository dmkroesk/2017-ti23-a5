#include <vs10xx.h>

// when the radio starts up this sound is played to show its alive
int AF_StartupBeep(void)
{
	int i;
	int ii = 1;
	//VsBeepStartRaw(400);
	while(1)
	{
			for(i = 2; i <5 ; i++)
			{
				VsPlayerInit();
				VsBeep(((ii+100) * (i + 1)), 500);
				VsPlayerStop();
			}
			break;

			printf("number of itterations is: %d\n",ii );
	}
	return 1;
}
// this sound is played when the volume is changed
int AF_VolumeBeep(void)
{
  VsPlayerInit();
  VsBeep(250, 1);
  VsPlayerStop();
}
// this sound is played when the max volume is reached
int AF_MaxVolume(void)
{
  int i = 0;

  for( i = 0; i <3 ; i++)
  {
    VsPlayerInit();
    VsBeep(((250) * (i + 1)), 600);
    VsPlayerStop();
  }
}

// this sound is played when a normal button is being pressed
int AF_ButtonBeep(void)
{
  VsPlayerInit();
  VsBeep(150   , 1);
  VsPlayerStop();
}
