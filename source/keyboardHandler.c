//
// Created by cezan on 20-Feb-17.
//
#include <stdio.h>
#include <sys/thread.h>
#include <sys/timer.h>



#include "keyboard.h"
#include "../include/keyboardHandler.h"
#include "menu/menu.h"
#include "../include/display.h"
#include "shoutcast.h"
#include "player.h"


#include "watchdog.h"

#include <time.h>
#include "rtc.h"
#include "menu/FirstStartup.h"
#include "vs10xx.h"
#include "menu/Volume.h"
#include "AudioFeedback.h"
#include "Alarm/alarm.h"

void KbhInit(Volume *volume)
{
	NutThreadSetPriority(1);
	createKeyThread(volume);
}


/*!
 * This thread is the keylisterner thread it asks the pressed key 10X p/s.
 * If the pressed key is usefull for the menu it tells the menu a key is pressed and which.
 *
 *
 * !*/
THREAD(KeyThread, arg){
    Volume* volume = (Volume*) arg;
    int justPressedKey = 0;
    double noKeyPress = 0.0;
    while(1)
    {
        NutSleep(100);
        int pressedKey = KbGetKey();
        //@TODO check where we need to send the input (menu/alarm)
        if(pressedKey != 136) // check button press
        {
            noKeyPress = 0;
            LcdBackLight(LCD_BACKLIGHT_ON);
            NutSleep(350);
            printf("pressed %i\n", pressedKey);
            if(pressedKey > 6 && pressedKey < 15)
                {
                    if(cont)
                        FirstStartup_HandleInput(pressedKey);
                    else
                        Menu_handle_input(pressedKey);


                    Menu_draw(true);
                    //printf("sending %i to handleButton()\n", pressedKey);
                    justPressedKey = pressedKey;
                    //AF_ButtonBeep();
                }
                else if(pressedKey == 2 || pressedKey == 5){
                  switch (pressedKey) {
                    case 5:
                        if (volume->volume < 250)
                        {
                           volume->volume = volume->volume + volume->increment;
                           volume->percentage = volume->percentage - 10;
                          // AF_VolumeBeep();
                         }
                         else
                           //AF_MaxVolume();


                        VsSetVolume(volume->volume,volume->volume);
                        FLASHSETTINGS_SAVE_SETTINGS();
                        break;
                    case 2:
                        if (volume->volume > 0)
                        {
                          volume->volume = volume->volume - volume->increment;
                          volume->percentage = volume->percentage + 10;
                          //AF_VolumeBeep();
                        }
                        VsSetVolume(volume->volume,volume->volume);
                        FLASHSETTINGS_SAVE_SETTINGS();
                        break;
                  }
                }
                else if(pressedKey > 0 && pressedKey < 7 && pressedKey != 2 && pressedKey != 5 ){
                  Menu_handle_input(pressedKey);
                  Menu_draw(true);
                  justPressedKey = pressedKey;
                  VsSetVolume(volume->volume,volume->volume);
                  switch (pressedKey) {
                    case 1:
                        //snooze
                        Alarm_snooze();
                        break;
                    case 4:
                        Alarm_stop();
                        break;
                      }
                  //AF_ButtonBeep();
                }
        }else
            noKeyPress += 0.1;
						if(noKeyPress >10){
							LcdBackLight(LCD_BACKLIGHT_OFF);
							Menu_handle_input(666);
						}
				}
			}
/*!
 * Creates the keyThread
 */
void createKeyThread(Volume *volume)
{
	NutThreadCreate("KeyThread", KeyThread, volume, 512);
	printf("KeyThread Made \n");
}
