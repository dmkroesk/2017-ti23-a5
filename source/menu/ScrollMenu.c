#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"
#include <string.h>
#include "menu/ScrollMenu__internal.h"

void ScrollMenu_handle_input(Menu *menu, int input)
{
	ScrollMenu *sm = menu->data; /// we extract the menu's data from the menu pointer

	switch (input)
	{
	case ESC: /// we return to the previus menu
		Menu_set(sm->prev);
		sm->prev->on_return(sm->prev);
		break;

	case OK: /// we enter the selected menu
		sm->menus[sm->index].on_enter(&(sm->menus[sm->index]), menu);
		Menu_set(&(sm->menus[sm->index]));
		break;

	case UP: /// we move our index up (we count from top to bottom)
		if (--sm->index < 0) /// if we've exited the array
		{
			sm->index = sm->length - 1; /// we loop around
		}
		break;

	case DOWN: /// we move our index down (we count from top to bottom)
		if (++sm->index >= sm->length) /// if we've exited the array
		{
			sm->index = 0; /// we loop around
		}
		break;
	}
}

void ScrollMenu_on_enter(Menu *menu, struct Menu *prev)
{
	//printf("ScrollMenu_on_enter() <start>\n");
    /// we set our previous pointer
	ScrollMenu *sm = menu->data;
	((ScrollMenu *)menu->data)->prev = prev;
	//printf("ScrollMenu_on_enter() <end>\n");
}


void ScrollMenu_on_return(Menu *menu)
{
    /// we reset our index
	ScrollMenu *sm = menu->data;
	((ScrollMenu *)menu->data)->index = 0;
}


void ScrollMenu_draw(Menu *menu)
{
	ScrollMenu *sm = menu->data;
//    printf("ScrollMenu_draw(): data=%p\n", sm);
//    printf("ScrollMenu_draw(): itemCount=%i index=%i\n", sm->length, sm->index);

	char str_buff[17]; /// our buffer, lcd length + 1 (for the '\n')

    /// we print the selected menu to the buffer
	sprintf(str_buff, " >%-14s", sm->menus[sm->index].name);

    /// we print the buffer to the lcd
	SetCursor(0, TOP);
	LcdString(str_buff, strlen(str_buff));

	SetCursor(0, BOTTOM);
	if ((sm->index + 1) < sm->length) /// if the menu index below us is valid
	{
		//      printf("ScrollMenu_draw(): strPtr=%p -> \"%s\"\n", sm->menus[sm->index+1].name, sm->menus[sm->index+1].name);
        /// we print the menu

			LcdString(str_buff, sprintf(str_buff, "  %-14s", sm->menus[sm->index + 1].name));
	}
	else /// if we are at the end of the array
	{
		if (sm->length > 1) /// and the array has more than one entry
		{
            /// we print the first entry to create a looping effect
			LcdString(str_buff, sprintf(str_buff, "  %-14s", sm->menus[0].name));
		}
		else
		{
            /// otherwise we ensure the bottom row is empty
			LcdString(str_buff, sprintf(str_buff, "  %-14s", "         "));
		}
	}
}


void ScrollMenu_InstaReturn_on_return(Menu *menu)
{
    /// on return we immediately return to our previous menu
	ScrollMenu *sm = menu->data;

	sm->prev->on_return(sm->prev);
	Menu_set(sm->prev);
}

/// the constructor
Menu *Menu_ScrollMenu(char *name, const Menu *const menus, int length)
{
    /// allocate the menu
	Menu *menu = malloc(sizeof(Menu));

	menu->name = name;

    /// allocate and set the data
	ScrollMenu *data = malloc(sizeof(ScrollMenu));
	data->menus  = menus;
	data->length = length;
	data->index  = 0;

	menu->data = data;

    /// set the function pointers
	menu->handle_input = ScrollMenu_handle_input;
	menu->on_enter     = ScrollMenu_on_enter;
	menu->on_return    = ScrollMenu_on_return;
	menu->draw         = ScrollMenu_draw;

	return menu;
}


Menu *Menu_ScrollMenu_InstaReturn(char *name, Menu *menus, int length)
{
    /// use the normal constructor, but override the on_return function
	Menu *temp = Menu_ScrollMenu(name, menus, length);

	temp->on_return = ScrollMenu_InstaReturn_on_return;
	return temp;
}
