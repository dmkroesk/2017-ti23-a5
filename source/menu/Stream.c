//
// Created by maxde on 24/02/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"
#include "menu/Stream.h"



void Stream_handle_input(Menu *menu, int input)
{
}


void Stream_on_enter(Menu *menu, struct Menu *prev)
{
}


void Stream_draw(Menu *menu)
{
}


Menu *TempMenu_stream(char *name, char *ip, int port)
{
   Menu *menu = malloc(sizeof(Menu));

   Stream *data = malloc(sizeof(Stream));

   data->name = name;
   data->ip   = ip;
   data->port = port;

   menu->data = data;
   menu->name = name;

   menu->handle_input = Stream_handle_input;
   menu->on_enter     = Stream_on_enter;
   menu->draw         = Stream_draw;

   return menu;
}
