//
// Created by maxde on 24/02/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"
#include "vs10xx.h"
#include "menu/Volume.h"


void Volume_handle_input(Menu * menu, int input)
{
    Volume *vol = menu->data;

    switch (input)
    {
        case ESC:
            vol->prev->on_return(vol->prev);
            Menu_set(vol->prev);
            break;
        case OK:
            //TODO save/enforce actual volume
            //returns to previous
            vol->prev->on_return(vol->prev);
            Menu_set(vol->prev);
            break;
        case DOWN:
            if (vol->volume < 250)
            {
               vol->volume = vol->volume + vol->increment;
               vol->percentage = vol->percentage - 10;
             }
            VsSetVolume(vol->volume,vol->volume);
            break;
        case UP:
            if (vol->volume > 0)
            {
              vol->volume = vol->volume - vol->increment;
              vol->percentage = vol->percentage + 10;
            }
            VsSetVolume(vol->volume,vol->volume);
            break;
    }
}

void Volume_on_enter(Menu * menu, struct Menu* prev)
{
    ((Volume*)menu->data)->prev = prev;
}

void Volume_draw(Menu * menu)
{
    Volume *vol = menu->data;

    char str_buff[17];
    sprintf(str_buff, " Volume: %-10i", vol->percentage);

    SetCursor(0, TOP);
    LcdString(str_buff, 17);
    SetCursor(0, BOTTOM);
    LcdString("                 ", 17);
}

Menu *Menu_Volume()
{
    Menu *menu = malloc(sizeof(Menu));

    Volume *data = malloc(sizeof(Volume));
    data->volume = 125;
    data->increment = 25;
    data->percentage = 50;

    menu->data = data;
    menu->name = "volume";
    menu->handle_input = Volume_handle_input;
    menu->on_enter = Volume_on_enter;
    menu->draw = Volume_draw;

    return menu;
}
