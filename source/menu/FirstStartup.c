/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     FirstStartup
 * [TITLE]      firststartup source file
 * [FILE]       FirstStartup.c
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV
 * [PURPOSE]    Screen that shows when the IMC boots for the first time
 *              or after a factoryreset. It asks for local GMT time.
 * ======================================================================== */
#define LOG_MODULE  LOG_DISPLAY_MODULE

#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"
#include "../include/menu/FirstStartup.h"
#include "rtc.h"


/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
int timezone = 0;
int GMT_TIME;
int cont = 0;
/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

void FirstStartup_Start(){
    cont = 1;
    FirstStartup_Draw();
}

/*
 *  Draws the first startup screen
 */

void FirstStartup_Draw() {
    if(cont) {
        LcdBackLight(LCD_BACKLIGHT_ON);
        SetCursor(1, TOP);
        LcdString("Set timezone: ", 14);
        SetCursor(5, BOTTOM);
        LcdString("GMT",3);
        if (timezone > 0)
            LcdChar('+');
        else if (timezone < 0) {
            // do nothing, negative value will be automaticly prepended with a minus sign
        } else
            LcdChar(' ');

        char stringTimezone;

        itoa(timezone, &stringTimezone, 10);
        if (timezone < 10 && timezone > -1) {
            LcdString(&stringTimezone, 1);
            LcdChar(' ');
            SetCursor(9, BOTTOM);
        } else if (timezone < -9) {
            LcdString(&stringTimezone, 3);
            SetCursor(10, BOTTOM);
        } else if(timezone>=10){
            LcdString(&stringTimezone,2);
            SetCursor(10, BOTTOM);
        } else {
            LcdString(&stringTimezone, 2);
            LcdChar(' ');
            SetCursor(9, BOTTOM);
        }
        BlinkCursor();
    }
}

/**
 * Handles the input received from
 * keyboardHandler
 */
void FirstStartup_HandleInput(int button){
    printf("StartUp button %i\n", button);
    switch (button) {
        case 8: // case UpArrow
            if (timezone != 12)
                timezone = timezone + 1;
            SetCursor(0,TOP);
            LcdClear();
            FirstStartup_Draw();
            break;
        case 9: // case OK button
            FirstStartup_Enter();
            break;
        case 11: // case DownArrow
            if (timezone != -12)
                timezone--;
            FirstStartup_Draw();
            break;
        default:
            break;
    }
}

void FirstStartup_Enter(){
    GMT_TIME = timezone;
    FLASHSETTINGS_SAVE_SETTINGS();
    LcdClear();
    SetCursor(0,TOP);
    LcdString("Connecting to",13);
    SetCursor(0,BOTTOM);
    LcdString("internet",8);
    CursorOff();
    cont = 0;
}


/* ---------- end of module ------------------------------------------------ */

/*@}*/
