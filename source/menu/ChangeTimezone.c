//
// Created by maxde on 24/02/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"

typedef struct
{
    char* placeholder;

    Menu *prev;
}Timezone;

void ChangeTimezone_handle_input(Menu * menu, int input)
{
    Timezone *timezone = menu->data;

    switch (input)
    {
        case ESC:
            timezone->prev->on_return(timezone->prev);
            Menu_set(timezone->prev);
            break;
        case OK:
            //TODO save/enforce actual volume
            timezone->prev->on_return(timezone->prev);
            Menu_set(timezone->prev);
            break;
        case UP:
            //TODO logic
            break;
        case DOWN:
            //TODO logic
            break;
    }
}

void ChangeTimezone_on_enter(Menu * menu, struct Menu* prev)
{
    ((Timezone*)menu->data)->prev = prev;
}

void ChangeTimezone_draw(Menu * menu)
{
    Timezone *timezone = menu->data;

    char str_buff[17];
    sprintf(str_buff, " %s", timezone->placeholder);

    SetCursor(0, TOP);
    LcdString(str_buff, 17);
    SetCursor(0, BOTTOM);
    LcdString("                 ", 17);
}

Menu *Menu_ChangeTimezone()
{
    Menu *menu = malloc(sizeof(Menu));\

    Timezone *data = malloc(sizeof(Timezone));
    data->placeholder = "placeholder    ";

    menu->data = data;
    menu->name = "change timezone";
    menu->handle_input = ChangeTimezone_handle_input;
    menu->on_enter = ChangeTimezone_on_enter;
    menu->draw = ChangeTimezone_draw;

    return menu;
}
