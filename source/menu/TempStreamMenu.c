#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"
#include <string.h>
#include "shoutcast.h"
#include "menu/Stream.h"

typedef struct
{
    Stream streams[3];
    int length;
    int index;
    int selected;

    Menu *prev;
}TempMenu;

void TempMenu_handle_input(Menu * menu, int input)
{
    TempMenu *sm = menu->data;

    switch (input)
    {
        case ESC:
            Menu_set(sm->prev);
            sm->prev->on_return(sm->prev);
            break;
        case OK:
            IP1 = sm->streams[sm->index].ip;
            Port = sm->streams[sm->index].port;
            Menu_set(sm->prev);
            sm->prev->on_return(sm->prev);
            break;
        case UP:
            if (--sm->index < 0) sm->index = sm->length - 1;
            break;
        case DOWN:
            if (++sm->index >= sm->length) sm->index = 0;
            break;
    }
}

void TempMenu_on_enter(Menu * menu, struct Menu* prev)
{
    ((TempMenu*)menu->data)->prev = prev;
}

void TempMenu_on_return(Menu * menu)
{
    ((TempMenu*)menu->data)->index = 0;
}

void TempMenu_draw(Menu * menu)
{
    TempMenu *sm = menu->data;

    char str_buff[17];
    sprintf(str_buff, " >%-14s", sm->streams[sm->index].name);

    SetCursor(0, TOP);
    LcdString(str_buff, strlen(str_buff));

    if (sm->index +1 < sm->length)
    {
        sprintf(str_buff, "  %-14s", sm->streams[sm->index+1].name);
    }
    else
    {
        if(sm->length > 1)
        {
            sprintf(str_buff, "  %-14s", sm->streams[0].name);
        }
        else
        {
            sprintf(str_buff, "  %-14s", "         ");
        }
    }


    SetCursor(0, BOTTOM);
    LcdString(str_buff, strlen(str_buff));
}

void TempMenu_InstaReturn_on_return(Menu *menu)
{
    TempMenu *sm = menu->data;
    sm->prev->on_return(sm->prev);
    Menu_set(sm->prev);
}

Menu *TempMenu_ScrollMenu(char * name, int length)
{
    Menu *menu = malloc(sizeof(Menu));
    menu->name = name;

    TempMenu *data = malloc(sizeof(TempMenu));
    Stream stream1;
    stream1.name ="Hubu.FM shoutcast";
    stream1.ip = "94.23.53.96";
    stream1.port = 500;
    Stream stream2;
    stream2.name ="Radio Prahova Manele";
    stream2.ip = "54.202.122.200";
    stream2.port = 8000;
    Stream stream3;
    stream3.name ="HITRADIO CENTER";
    stream3.ip = "94.23.53.96";
    stream3.port = 500;
    data->streams[0] = stream1;
    data->streams[1] = stream2;
    data->streams[2] = stream3;

    data->length = length;
    data->index = 0;

    menu->data = data;

    menu->handle_input = TempMenu_handle_input;
    menu->on_enter = TempMenu_on_enter;
    menu->on_return = TempMenu_on_return;
    menu->draw = TempMenu_draw;

    return menu;
}

Menu *TempMenu_InstaReturn(char *name, Menu *menus, int length)
{
    Menu *temp = Menu_ScrollMenu(name, menus, length);
    temp->on_return = TempMenu_InstaReturn_on_return;
    return temp;
}
