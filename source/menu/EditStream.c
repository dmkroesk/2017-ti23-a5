//
// Created by maxde on 24/02/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"

typedef struct
{
    char* placeholder;

    Menu *prev;
}Stream;

void EditStream_handle_input(Menu * menu, int input)
{
    Stream *stream = menu->data;

    switch (input)
    {
        case ESC:
            stream->prev->on_return(stream->prev);
            Menu_set(stream->prev);
            break;
        case OK:
            //TODO save/enforce actual volume
            stream->prev->on_return(stream->prev);
            Menu_set(stream->prev);
            break;
        case UP:
            //TODO logic
            break;
        case DOWN:
            //TODO logic
            break;
    }
}

void EditStream_on_enter(Menu * menu, struct Menu* prev)
{
    ((Stream*)menu->data)->prev = prev;
}

void EditStream_draw(Menu * menu)
{
    Stream *stream = menu->data;

    char str_buff[17];
    sprintf(str_buff, " %s", stream->placeholder);

    SetCursor(0, TOP);
    LcdString(str_buff, 17);
    SetCursor(0, BOTTOM);
    LcdString("                 ", 17);
}

Menu *Menu_EditStream()
{
    Menu *menu = malloc(sizeof(Menu));\

    Stream *data = malloc(sizeof(Stream));
    data->placeholder = "EDIT STREAM";

    menu->data = data;
    menu->name = "edit stream";
    menu->handle_input = EditStream_handle_input;
    menu->on_enter = EditStream_on_enter;
    menu->draw = EditStream_draw;

    return menu;
}
