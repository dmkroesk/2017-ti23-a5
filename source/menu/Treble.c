//
// Created by maxde on 24/02/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"
#include "vs10xx.h"
#include "menu/EQ.h"


/*
    This function handles the input when the menu is on the Treble menu
    input:
    Menu * menu: a pointer to the Treble Menu struct, this struct holds the all
    the information data of this menu
    int input: the value of the button which is pressed
*/
void Treble_handle_input(Menu * menu, int input)
{
    //the data struct of Treble menu specific data
    EQ *treble = menu->data;

    //switch case for input
    switch (input)
    {
        case ESC:
            //returns to the previous menu
            treble->prev->on_return(treble->prev);
            Menu_set(treble->prev);
            break;
        case OK:
            //returns to the previous menu
            treble->prev->on_return(treble->prev);
            Menu_set(treble->prev);
            break;
        case UP:
            //if within limits the treble can be turned up, it will be turned up
              if (treble->treble < 7)
            {
              //turning the treble up
               treble->treble = treble->treble + treble->tincrement;
               // lower bass frequency limit by 10Hz steps | enhance bass by 1dB steps | raises treble frequency limit by 1k Hz steps | enhance treble by 1.5dB steps
               uint16_t total = treble->bass | (treble->bass<<4) | (0<<8) | (treble->treble<<12);
               //This writes the 2-byte int to registry, the registry 0x2 is the bass and treble registry used by the vs1003b sound chip
               VsRegWrite(0x2,total);
             }
            break;
        case DOWN:
            //if within limits the treble can be turned down, it will be turned down
            if (treble->treble > 0)
            {
              //turning the treble down
              treble->treble = treble->treble - treble->tincrement;
              // lower bass frequency limit by 10Hz steps | enhance bass by 1dB steps | raises treble frequency limit by 1k Hz steps | enhance treble by 1.5dB steps
              uint16_t total = treble->bass | (treble->bass<<4) | (0<<8) | (treble->treble<<12);
              //This writes the 2-byte int to registry, the registry 0x2 is the bass and treble registry used by the vs1003b sound chip
              VsRegWrite(0x2,total);
            }
            break;
    }
}

/*
    This functions is called when this Menu is entered
    input:
    Menu * menu: a pointer to the Treble Menu struct, this struct holds the all
    the information data of this menu
    struct Menu* prev: a point to the previous Menu struct
*/
void Treble_on_enter(Menu * menu, struct Menu* prev)
{
    //sets the previous menu to the previous menu given in the function
    ((EQ*)menu->data)->prev = prev;
}

/*
    This functions is called when this Menu needs to be drawn
    input:
    Menu * menu: a pointer to the Treble Menu struct, this struct holds the all
    the information data of this menu
*/
void Treble_draw(Menu * menu)
{
    //the data struct of Bass menu specific data
    EQ *treble = menu->data;

    //makes a char* for showing the bass level on the display
    char str_buff[17];
    sprintf(str_buff, " Treble: %-10i", treble->treble);

    //sets cursor to top row, first block
    SetCursor(0, TOP);
    //writes the char* on display
    LcdString(str_buff, 17);
    //sets cursor to bottom row, first block
    SetCursor(0, BOTTOM);
    //writes the bottom row empty in case there is something written there
    LcdString("                 ", 17);
}

/*
    This functions serves as a "constructor", generating a Menu struct for a Treble menu
    input:
    EQ *data: a pointer to a struct which holds the treble and bass
    output:
    a pointer to the Treble Menu struct, this struct holds the all
    the information data of this menu
*/
Menu *Menu_Treble(EQ *data)
{
    //mallocs the menu struct so it can be used in the program
    Menu *menu = malloc(sizeof(Menu));


    //sets bass level to standard 0
    data->treble = 0;
    //sets bass increment to 1
    data->tincrement = 1;

    //sets the menu specific data as the treble bass struct
    menu->data = data;
    //names this Menu
    menu->name = "treble";
    //sets the Menu functions to the functions above
    menu->handle_input = Treble_handle_input;
    menu->on_enter = Treble_on_enter;
    menu->draw = Treble_draw;

    //returns a Menu*
    return menu;
}
