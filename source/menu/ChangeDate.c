//
// Created by maxde on 24/02/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"

typedef struct
{
    char* placeholder;

    Menu *prev;
}Date;

void ChangeDate_handle_input(Menu * menu, int input)
{
    Date *date = menu->data;

    switch (input)
    {
        case ESC:
            date->prev->on_return(date->prev);
            Menu_set(date->prev);
            break;
        case OK:
            //TODO save/enforce actual volume
            date->prev->on_return(date->prev);
            Menu_set(date->prev);
            break;
        case UP:
            //TODO logic
            break;
        case DOWN:
            //TODO logic
            break;
    }
}

void ChangeDate_on_enter(Menu * menu, struct Menu* prev)
{
    ((Date*)menu->data)->prev = prev;
}

void ChangeDate_draw(Menu * menu)
{
    Date *date = menu->data;

    char str_buff[17];
    sprintf(str_buff, " %s", date->placeholder);

    SetCursor(0, TOP);
    LcdString(str_buff, 17);
    SetCursor(0, BOTTOM);
    LcdString("                 ", 17);
}

Menu *Menu_ChangeDate()
{
    Menu *menu = malloc(sizeof(Menu));

    Date *data = malloc(sizeof(Date));
    data->placeholder = "placeholder    ";

    menu->data = data;

    menu->name = "change date";
    menu->handle_input = ChangeDate_handle_input;
    menu->on_enter = ChangeDate_on_enter;
    menu->draw = ChangeDate_draw;

    return menu;
}
