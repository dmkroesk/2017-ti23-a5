//
// Created by maxde on 24/02/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>
#include "display.h"

typedef struct
{
    char* placeholder;

    Menu *prev;
}Stream;

void DeleteStream_handle_input(Menu * menu, int input)
{
    Stream *stream = menu->data;

    switch (input)
    {
        case ESC:
            stream->prev->on_return(stream->prev);
            Menu_set(stream->prev);
            break;
        case OK:
            //TODO save/enforce actual volume
            stream->prev->on_return(stream->prev);
            Menu_set(stream->prev);
            break;
        case UP:
            //TODO logic
            break;
        case DOWN:
            //TODO logic
            break;
    }
}

void DeleteStream_on_enter(Menu * menu, struct Menu* prev)
{
    ((Stream*)menu->data)->prev = prev;
}

void DeleteStream_draw(Menu * menu)
{
    Stream *stream = menu->data;

    char str_buff[17];
    sprintf(str_buff, " %s", stream->placeholder);

    SetCursor(0, TOP);
    LcdString(str_buff, 17);
    SetCursor(0, BOTTOM);
    LcdString("                 ", 17);
}

Menu *Menu_DeleteStream()
{
    Menu *menu = malloc(sizeof(Menu));\

    Stream *data = malloc(sizeof(Stream));
    data->placeholder = "placeholder    ";

    menu->data = data;
    menu->name = "delete stream";
    menu->handle_input = DeleteStream_handle_input;
    menu->on_enter = DeleteStream_on_enter;
    menu->draw = DeleteStream_draw;

    return menu;
}
