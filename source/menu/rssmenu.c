/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     RSS_MENU
 * [TITLE]      RSS_MENU source file
 * [FILE]       RSS_MENU.c
 * [VSN]        1.0
 * [PURPOSE]    RSS_MENU
 * ======================================================================== */
#define RSS_MENU    RSS_MENU

#include "menu/rssmenu.h"
#include "shoutcast.h"
#include "uxml.h"
/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
int show_headlines = 0;
/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
void handle_rss_input(Menu *this, int input);
void on_enter_rss(Menu *this, Menu *prev);
void draw_rss(Menu *this);

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

FILE *rss_connect(char *ip, const int port, const char *route)
{
	FILE * data = connect_to_rss(ip, port, route);
}

void handle_rss_input(Menu *this, int input)
{
	//TODO do input handling.
	rss *rss_data = this->data;

	switch (input)
	{
	case ESC:
		show_headlines = 0;
		rss_data->prev->on_return(rss_data->prev);
		Menu_set(rss_data->prev);
		break;

	case OK:
		show_headlines = 1;
		FILE * temp = connect_to_rss("136.144.128.124", 80, "/rss/");
		draw_rss(this);
		break;

	case UP:
		break;

	case DOWN:
		break;
	}
}


void on_enter_rss(Menu *this, Menu *prev)
{
	((rss *)this->data)->prev = prev;
}


void draw_rss(Menu *this)
{
	if (show_headlines)
	{
		SetCursor(0, TOP);
		LcdString("Boeven ontsnapt!", 16);
		SetCursor(0, BOTTOM);
		LcdString("Boeven gevangen!", 16);
	}
	else
	{
		char str_buff[16];
		rss *rss_data = this->data;

		sprintf(str_buff, "%s", rss_data->title);

		SetCursor(0, TOP);
		LcdString(str_buff, 16);
		SetCursor(0, BOTTOM);
		LcdString("Press OK to read", 16);
	}
}


Menu *Menu_Rss(void)
{
	Menu *menu = malloc(sizeof(Menu));

	menu->name = "Rss stream";
	rss *data = malloc(sizeof(rss));
	data->title = "     Het AD     ";
	menu->data  = data;

	menu->handle_input = handle_rss_input;
	menu->on_enter     = on_enter_rss;
	menu->draw         = draw_rss;

	return menu;
}


/* ---------- end of RSS_MENU ------------------------------------------------ */
