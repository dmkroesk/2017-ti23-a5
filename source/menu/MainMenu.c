//
// Created by flori on 2/24/2017.
//

#include "menu/menu__internal.h"
#include <stdlib.h>
#include <stdio.h>

#include <sys/timer.h>
#include <stdbool.h>
#include "display.h"
#include "rtc.h"
#include "shoutcast.h"
#include "player.h"
#include "vs10xx.h"

#include "alarm/queue.h"

typedef struct
{
	Menu *next;

	tm   mainscreenGmt;
	char mainscreenTime[8];
	bool internetSet;
	bool alarmSet;
	bool volumeSet;
} MainMenu;

int playing = 0;

void MainMenu_handle_input(Menu *menu, int input)
{
	MainMenu *mm = menu->data;

	switch (input)
	{
	case DOWN:
		mm->next->on_enter(mm->next, menu);
		Menu_set(mm->next);
		break;

	case 3:
		if(playing){
			Stop();
			stopStream();
			playing = 0;
		}
		else{
			printf("connecting to stream\n");
			if (Internet)
			{
				connectToStream();
				NutSleep(300);
				playStream();
				NutSleep(1000);
				VsPlayerKick();
				playing = 1;
			}
		}
		break;
	}
}


void MainMenu_on_return(Menu *menu)
{
	LcdClear();
}


void MainMenu_draw(Menu *menu)
{
	MainMenu *mm = menu->data;
	u_short  vol = VsGetVolume();

	vol &= 0xff;

	//LcdClear();

	if (Internet)
	{
		SetCursor(0, TOP);
		LcdCustomChar('4');
	}
	// TODO check for alarm set
	if (queuePtr->alarm)
	{
		SetCursor(0, BOTTOM);
		LcdCustomChar('3');
		char buffer[6];
		sprintf(buffer, "%02i:%02i", queuePtr->alarm->alarmTime.tm_hour, queuePtr->alarm->alarmTime.tm_min);
		LcdString(buffer, 5);
	}

	SetCursor(14, BOTTOM);
	LcdCustomChar('0');
	if (vol <= 0)
	{
		LcdCustomChar('1');
	}
	else if (vol >= 250)
	{
		LcdChar('x');
	}
	else
	{
		LcdCustomChar('2');
	}

	// display the time
	SetCursor(4, TOP);
	LcdString(GetTime((char *)mm->mainscreenTime, mm->mainscreenGmt), 8);
}


Menu *Menu_MainMenu(Menu *next)
{
	Menu *menu = malloc(sizeof(Menu));

	MainMenu *mainMenu = malloc(sizeof(MainMenu));

	mainMenu->next = next;

	menu->data         = mainMenu;
	menu->handle_input = MainMenu_handle_input;
	menu->on_return    = MainMenu_on_return;
	menu->draw         = MainMenu_draw;

	return menu;
}
