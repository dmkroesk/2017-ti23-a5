/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     Menu
 * [TITLE]      misplay source file
 * [FILE]       menu.c
 * [VSN]        1.0
 * [PURPOSE]    Move through menus and adjust values
 * ======================================================================== */

#define LOG_MODULE  LOG_DISPLAY_MODULE

#include <stdbool.h> // bool type
#include <stdio.h> // printf()

#include "menu/menu__internal.h"
#include "log.h"

static Menu *current = NULL; /// the menu we are currently in
static Menu *main = NULL; /// our 'base' or 'main' menu, for usage see Menu_handle_input(), Menu_draw()
static bool has_init = false; /// has Menu_init been called, for usage see Menu_init()

extern void Menu_set(Menu *menu)
{
    if (menu == NULL) /// if the proposed menu is NULL
    {
        /// we display an error
        printf("[%s %s] menu must be != NULL", __FILE__, __FUNCTION__);
    }
    /// otherwise we set the menu
    /// NOTE the caller is responsible for calling on_enter(), and on_return()
    current = menu;
}

extern void Menu_init(Menu *menu)
{
    if (has_init) /// if this method has been called already
    {
        // we display an error
        printf("[%s %s] Menu has init\n", __FILE__, __FUNCTION__);
        return;
    }
    /// otherwise we initialize the menu pointers
    main = current = menu;

}

extern void Menu_handle_input(int input)
{
    if (input == 666) /// if the input indicates an expiration. TODO define this number
    {
        if (current == main) /// if we are already in the main menu
            return;

        /// otherwise we set the current menu to the main menu
        main->on_return(main);
        current = main;
        Menu_draw(true);
    }
    else if(input == 2 || input == 5) ; /// if the inputs are volume control ignore them TODO define these numbers
    else if (current) current->handle_input(current, input); /// otherwise we let the menu handle the input
}

extern void Menu_draw(bool force)
{
    if (!current) return; /// if we have no menu we can't draw it
    if (!force && (current != main)) return; /// Menu_draw() gets called in two places after handling an input (when current may have changed), and every second (to update the time).
                                             /// so we need to draw the menu when we are forced to(input) or when we are displaying the main menu(time update)
    current->draw(current);
}
