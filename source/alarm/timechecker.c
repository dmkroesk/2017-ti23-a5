/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     Mainscreen
 * [TITLE]      mainscreen source file
 * [FILE]       mainscreen.c
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV
 * [PURPOSE]    Main menu TODO
 * ======================================================================== */
#define LOG_MODULE    LOG_DISPLAY_MODULE

#include "alarm/timechecker.h"
/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/
// 1 > 2
//evaluate 2 tm struct, return 1 if tm1 is bigger, 0 if tm2 is bigger.
int isbigger_tm(tm tm1, tm tm2)
{
	if (tm1.tm_year < tm2.tm_year)
	{
		return 0;
	}
	else if (tm1.tm_year > tm2.tm_year)
	{
		return 1;
	}
	if (tm1.tm_mon < tm2.tm_mon)
	{
		return 0;
	}
	else if (tm1.tm_mon > tm2.tm_mon)
	{
		return 1;
	}

	if (tm1.tm_mday < tm2.tm_mday)
	{
		return 0;
	}
	else if (tm1.tm_mday > tm2.tm_mday)
	{
		return 1;
	}
	if (tm1.tm_hour < tm2.tm_hour)
	{
		return 0;
	}
	else if (tm1.tm_hour > tm2.tm_hour)
	{
		return 1;
	}
	if (tm1.tm_min < tm2.tm_min)
	{
		return 0;
	}
	else if (tm1.tm_min > tm2.tm_min)
	{
		return 1;
	}
	if (tm1.tm_sec < tm2.tm_sec)
	{
		return 0;
	}
	else if (tm1.tm_sec > tm2.tm_sec)
	{
		return 1;
	}
	return 1;
}


//return 1 if tm1 == tm2, 0 otherwise
int equals_tm(tm tm1, tm tm2)
{
	// printf("Checking equality : \n");
	// printf("tm1 year: <%i>        -         <%i>\n", tm1.tm_year, tm2.tm_year);
	// printf("tm1 mon:  <%i>        -         <%i>\n", tm1.tm_mon, tm2.tm_mon);
	// printf("tm1 mday: <%i>        -         <%i>\n", tm1.tm_mday, tm2.tm_mday);
	// printf("tm1 hour: <%i>        -         <%i>\n", tm1.tm_hour, tm2.tm_hour);
	// printf("tm1 wday: <%i>        -         <%i>\n", tm1.tm_wday, tm2.tm_wday);
	if ((tm1.tm_wday == tm2.tm_wday) &&   //check weekday
		 (tm1.tm_year == tm2.tm_year) &&   //check year
		 (tm1.tm_mon == tm2.tm_mon) &&     //check month
		 (tm1.tm_mday == tm2.tm_mday) &&   //check monthday
		 (tm1.tm_hour == tm2.tm_hour))     //check hour, since we setup alarms with hours
	{
		return 1;
	}
	return 0;
}


/* ---------- end of module ------------------------------------------------ */

/*@}*/
