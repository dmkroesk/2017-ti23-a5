/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     alarmqueue
 * [TITLE]      alarmqueue source file
 * [FILE]       alarmqueue.c
 * [VSN]        1.0
 * [PURPOSE]    alarmqueue
 * ======================================================================== */

#include <stdio.h>
#include "rtc.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "alarm/alarm.h"
#include "alarm/queue.h"
#include "alarm/timechecker.h"

/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/

queue alarmqueue;
queue *queuePtr;
bool has_init = false;
/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/*
*@summary function that needs to be called before first usage of a queue.
*initializes a queue, sets the alarm and next node to null for further use.
*/
void init_queue()
{
	if (has_init)
	{
		return;
	}
	queuePtr        = &alarmqueue;
	queuePtr->alarm = NULL;
	queuePtr->next  = NULL;
	has_init         = true;
}

/*
*@summary Function to enqueue a alarm in the priority queue.
*@params Alarm pointer, Alarm to be put into the queue.
*/
void queue_enqueue(Alarm *newalarm)
{
	printf("Called enqueue \n");
	if (!has_init)
	{
		printf("Queue not yet initiated yet, call init_queue\n");
		return;
	}

	tm now;
	X12RtcGetClock(&now);

	if (isbigger_tm(now, newalarm->alarmTime)) //Check to see if alarm is later then current time.
	{
		//error, alarm on a passed date.
		printf("alarm date already passed\n");
		return;
	}
	if (queuePtr->alarm == NULL) //See if queue has a alarm in it, if not put it in first spot.
	{
		queuePtr->alarm = newalarm;
		printf("Set first in queue\n");
		signal_changed_alarm(&(queuePtr->alarm->alarmTime));  //Sets a flag and a time to trigger for the rtcnotifier service.
	}
	else
	{
		if (isbigger_tm(queuePtr->alarm->alarmTime, newalarm->alarmTime)) //There is a alarm in first spot, check if new alarm is earlier then the first alarm.
		{
			queue *newnode = malloc(sizeof(queue)); //Request memory for a new queue node.
			newnode->alarm = newalarm; //sets the queue node's members.
			newnode->next  = queuePtr; //set the next node to currents first.
			queuePtr       = newnode;  //Swap the current queue node with the newly created queue node.
			printf("Set first in queue.\n");
			signal_changed_alarm(&(queuePtr->alarm->alarmTime));  //Sets a flag and a time to trigger for the rtcnotifier service.
		}
		else
		{
			queue *prevPtr    = queuePtr; //Hold the current queuenode
			queue *currentPtr = queuePtr->next; //Hold the next queuenode
			if (currentPtr) //Check to see if a second alarm exists.
			{
				int count = 2; //We are checking after the second alarm so we start at 2.
				while (currentPtr) //While we have a next alarm, evaluate it.
				{
					if (isbigger_tm(currentPtr->alarm->alarmTime, newalarm->alarmTime)) //Is the checked alarm later then the new alarm, swap them.
					{
						queue *newnode = malloc(sizeof(queue));
						newnode->alarm = newalarm;
						newnode->next  = currentPtr;
						prevPtr->next  = newnode;
						return;
					}
					prevPtr    = currentPtr; //new alarm is later then checked alarm, update the prev pointer
					currentPtr = currentPtr->next; //Set the currentPtr to the next so we continue in the queue.
					count++;
				}
				//We found no alarm that is later then the new alarm, set the new alarm at the end of the queue.
				queue *newnode = malloc(sizeof(queue));
				newnode->alarm = newalarm;
				newnode->next  = NULL;
				prevPtr->next  = newnode;
				printf("Set <%i> in queue\n", count);
			}
			else
			{ //No second alarm set.
				//put after first, it is later then first
				queue *newnode = malloc(sizeof(queue));
				newnode->alarm = newalarm;
				newnode->next  = NULL;

				queuePtr->next = newnode;
				printf("Set second in queue!\n");
			}
		}
	}
}

/*
*@summary Function to dequeue a alarm in the priority queue.
*/
Alarm *queue_dequeue()
{
	if (!has_init)
	{
		printf("#ERROR# Queue not yet initiated yet, call init_queue#ERROR#\n");
		return;
	}
	Alarm *returnAlarm;
	if (isEmpty())
	{
		return;
	}
	else
	{
		if (queuePtr->next) //Is there a second alarm set? Swap it and notify the rtc service.
		{
			returnAlarm = queuePtr->alarm;
			queuePtr    = queuePtr->next;
			signal_changed_alarm(&(queuePtr->alarm->alarmTime));   //Sets a flag and a time to trigger for the rtcnotifier service.
			return returnAlarm;
		}
		else
		{
			if (queuePtr->alarm != NULL)
			{
				returnAlarm     = queuePtr->alarm;
				queuePtr->alarm = NULL;
				return returnAlarm;
			}
		}
	}
}

/*
*@summary Function to remove a alarm in the priority queue.
*@params tm struct, time to remove a alarm.
*/
void queue_remove(tm alarm)
{
	printf("Called queue_remove!\n");
	if (!has_init)
	{
		printf("#ERROR# Queue not yet initiated yet, call init_queue#ERROR#\n");
		return;
	}
	if (equals_tm(queuePtr->alarm->alarmTime, alarm)) //is the first alarm the to be found alarm
	{
		printf("First alarm is equal \n");
		// @TODO flash remove
		queuePtr->alarm = NULL;                               //reset it, incase it has no next.

		if (queuePtr->next) //has a next alarm
		{
			printf("next alarm is available \n");
			queue *temp = queuePtr;
			queuePtr = queuePtr->next;
			free(temp);
			signal_changed_alarm(&(queuePtr->alarm->alarmTime));  //Sets a flag and a time to trigger for the rtcnotifier service.
			return;        //found alarm so quit.
		}else{	//Not a next alarm so make sure the rtc notifier is cleared..

			printf("next alarm is null \n");
			tm *gmt = malloc(sizeof(tm));
			X12RtcGetClock(gmt);
			gmt->tm_year += 10;
			gmt->tm_sec = 10;

			signal_changed_alarm(gmt); //Tell the RTCnotifier to check a ubsurdly late time! TODO Better reset.
		}
	}
	queue *prevPtr    = queuePtr;
	queue *currentPtr = queuePtr->next;
	int   count       = 0;
	while (currentPtr)
	{
		printf("Checking with alarm: %i\n", count);
		if (equals_tm(currentPtr->alarm->alarmTime, alarm))//check if alarm is the to be found alarm
		{
			printf("Deleting alarm\n");
			prevPtr->next = currentPtr->next;       //wire the prev next Ptr to the node after the to be deleted alarm
			//free(currentPtr);  //free the queuenode with the to be deleted alarm.
			return;  //found alarm so quit.
		}
		count++;
		prevPtr    = currentPtr;
		currentPtr = currentPtr->next;
	}
}


int isEmpty()
{
	if (!has_init)
	{
		printf("#ERROR# Queue not yet initiated yet, call init_queue#ERROR#\n");
		return;
	}
	if (queuePtr->alarm == NULL)
	{
		return 1;                                          //true, empty
	}
	return 0;                                             //false, alarm existent..

//Can you do return(queuePtr->alarm) ?
}


void show_queue()
{
	queue *tempPtr = queuePtr;

	printf("------------------------\n");
	while (tempPtr)
	{
		printf("year    : [%i]\n", tempPtr->alarm->alarmTime.tm_year);
		printf("month   : [%i]\n", tempPtr->alarm->alarmTime.tm_mon);
		printf("day     : [%i]\n", tempPtr->alarm->alarmTime.tm_mday);
		printf("hours   : [%i]\n", tempPtr->alarm->alarmTime.tm_hour);
		printf("minutes : [%i]\n", tempPtr->alarm->alarmTime.tm_min);
		printf("seconds : [%i]\n", tempPtr->alarm->alarmTime.tm_sec);
		printf("------------------------\n");
		tempPtr = tempPtr->next;
	}
}


/* ---------- end of module ------------------------------------------------ */

/*@}*/
