#include <stdlib.h>
#include "alarm/alarm.h"
#include "menu/menu.h"
#include "rtc.h" // to get current time
#include <time.h> // mktime(), gmtime(), tm, time_t

#define ALARM_PERIODIC_VERBOSE 1

void print_tm(tm tm)
{
    if (ALARM_PERIODIC_VERBOSE) printf("y=%04i m=%02i d=%02i h=%02i m=%02i s=%02i\n", tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

void Alarm_Periodic_calculate_next_time(Alarm *alarmPtr)
{
	tm gmt;
	tm timestruct;

	X12RtcGetClock(&gmt);
	timestruct = gmt;

    if (ALARM_PERIODIC_VERBOSE) printf("Alarm_Periodic_calculate_next_time\n");

	switch (alarmPtr->state )
	{
	case IDLE:


		timestruct.tm_hour = alarmPtr->alarmTime.tm_hour;
    timestruct.tm_min = alarmPtr->alarmTime.tm_min;
    timestruct.tm_sec = alarmPtr->alarmTime.tm_sec;

        if (ALARM_PERIODIC_VERBOSE) {
            printf("gmt       : ");
            print_tm(gmt);
            printf("alarm_time: ");
            print_tm(timestruct);
        }
        if (isbigger_tm(gmt, timestruct))
        {// if the alarm is in the past
            if (ALARM_PERIODIC_VERBOSE) printf("alarm is in the past, so we set it to tommorow\n");
            // set the alarm for the next day, and check if the date is valid TODO better check
            if (++timestruct.tm_mday > 31)
            {
                timestruct.tm_mon++;
                timestruct.tm_mday = 1;
            }
        }
        else
        if (ALARM_PERIODIC_VERBOSE) printf("alarm is later today\n");

        if (ALARM_PERIODIC_VERBOSE) printf("modified alarmt_time: ");
        print_tm(timestruct);
        alarmPtr->alarmTime = timestruct;
		break;

	case SNOOZED:
		printf("%s\n", "snooze called");
		if (alarmPtr->snooze_count < 10)
		{
      print_tm(timestruct);
			timestruct.tm_sec += 15;
			alarmPtr->snooze_count++;
		}
		break;
	}


//check overflows, correc them. mktime does not worked.
	if (timestruct.tm_sec >= 61)
	{
		timestruct.tm_min++;
		timestruct.tm_sec = 1;
	}
	if (timestruct.tm_min == 60)
	{
		timestruct.tm_hour++;
		timestruct.tm_min = 1;
	}
	//mktime(&timestruct);        //does not work huh?
	printf("Result mktime: %i:%i:%i:%i:%i\n", timestruct.tm_mon, timestruct.tm_mday, timestruct.tm_hour, timestruct.tm_min, timestruct.tm_sec);
	alarmPtr->alarmTime = timestruct;
  print_tm(alarmPtr->alarmTime);
}


void Alarm_Periodic_getName(Alarm *alarm, char *dst)
{
	sprintf(dst, "%02i:%02i %c", alarm->alarmTime.tm_hour, alarm->alarmTime.tm_min, alarm->state == PLAYING ? 1 : ' ');
}



Alarm *Alarm_Periodic(tm alarm_time, Stream *stream)
{
	Alarm *alarm = malloc(sizeof(Alarm));

	alarm->calculate_next_time = Alarm_Periodic_calculate_next_time;
	alarm->alarmTime           = alarm_time;
	time_t secondsepoch = mktime(&alarm_time);
	alarm->alarmSeconds = secondsepoch;
	alarm->stream       = stream;
  alarm->state = IDLE;
  alarm->snooze_count = 0;

  alarm->calculate_next_time(alarm);


  alarm->menu = Alarm_EditDelete(alarm->alarmTime);
	alarm->getName = Alarm_Periodic_getName;
	return alarm;
}
