//
// Created by flori on 2017-03-14.
//

#include "alarm/alarm_internal.h"
#include "menu/menu__internal.h"
#include "alarm/queue.h"

#include "menu/ScrollMenu__internal.h"

#include <stdlib.h>

#define MENU_COUNT 2

Menu *menu = NULL;
Menu menus[MENU_COUNT + 1];
ScrollMenu *data;
Menu *newAlarm;


void print_tm2(tm tm)
{
  printf("y=%04i m=%02i d=%02i h=%02i m=%02i s=%02i\n", tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
}


void alarms_menu__genAlarms(void)
{
    queue *ptr = queuePtr;
    int i = 1;
    print_tm2(ptr->alarm->alarmTime);
    printf("Checking queue : \n");
    show_queue();
    printf("generating alarms\n");
    if (ptr != NULL && ptr->alarm != NULL)
    {
        while (ptr && i <= MENU_COUNT)
        {
            menus[i] = *(ptr->alarm->menu);
            menus[i].name = malloc(32);
            ptr->alarm->getName(ptr->alarm, menus[i].name);
            ptr = ptr->next;
            i++;
        }
        //printf("p loop\n");
    }
    printf("alarms_menu__genAlarms(): count=%i\n", i);
    if (i <= MENU_COUNT)
    {
        menus[0] = *newAlarm;
        data->length = i;
    }
    else
    {
        int j;
        for (j = 0; j < MENU_COUNT; j++)
        {
            menus[j] = menus[j+1];
        }
        data->length = MENU_COUNT;
    }
    data->index = 0;
}

void alarms_menu__on_enter(Menu *menu, Menu *prev)
{
    alarms_menu__genAlarms();
    ScrollMenu_on_enter(menu, prev);
}

void alarms_menu__on_return(Menu *menu)
{
    alarms_menu__genAlarms();
    ScrollMenu_on_return(menu);
}

Menu* get_alarmsMenu(void)
{
    if (menu) return menu;

    newAlarm = Alarm_NewAlarm();
    menu = Menu_ScrollMenu("Alarms", menus, 1);
    data = menu->data;

    menu->on_enter = alarms_menu__on_enter;
    menu->on_return = alarms_menu__on_return;

    return menu;
}
