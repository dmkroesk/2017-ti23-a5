//
// Created by flori on 2017-03-07.
//
#include <sys/timer.h>
#include "alarm/alarm.h"
#include "alarm/queue.h"
#include "shoutcast.h"
#include "player.h"

Alarm *current;
void signal_changed_alarm(tm *time_to_trigger)
{
	newalarmflag.trigger_time = time_to_trigger;
	newalarmflag.flag         = 1;
}


void Alarm_start()
{
	Alarm *tochip = queue_dequeue();

	if (current != 0)
	{
		current->state = IDLE;
		tochip->calculate_next_time(current);
		queue_enqueue(tochip);
		return;
	}
	Stop();
	stopStream();

	NutSleep(500);

	current        = tochip;
	current->state = PLAYING;
	printf("alarm started With trigger time : %i:%i;%i\n", current->alarmTime.tm_hour, current->alarmTime.tm_min, current->alarmTime.tm_sec);


	setipport("94.23.53.96", 500);
	if (Internet)
	{
		printf("Trying online stream \n");
		//setipport(tochip->stream->ip, tochip->stream->port);
		connectToStream();
		playStream();
		NutSleep(1000);
		VsPlayerKick();
	}
	else
	{
		printf("Trying beep\n");
		playBeep();                                              //check internet variable.
	}
}


void Alarm_stop()
{
	if (current == 0)
	{
		return;
	}
	printf("alarm stopped With trigger time : %i:%i;%i\n", current->alarmTime.tm_hour, current->alarmTime.tm_min, current->alarmTime.tm_sec);

	// stop stream
	Stop();
	stopStream();

	current->state = IDLE;
	current->calculate_next_time(current);
	// throw it back into the queue
	queue_enqueue(current);
	current = 0;
}


void Alarm_snooze()
{
	if (current == 0)
	{
		return;
	}

	// stop stream
	Stop();
	stopStream();

	current->state = SNOOZED;
	current->calculate_next_time(current);
	// throw it back into the queue
	queue_enqueue(current);
	current = 0;
}
