/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     alarmqueue
 * [TITLE]      alarmqueue source file
 * [FILE]       alarmqueue.c
 * [VSN]        1.0
 * [PURPOSE]    alarmqueue
 * ======================================================================== */

 #include <stdio.h>
 #include <string.h>
 #include <stdlib.h>
 #include <stdbool.h>
 #include "alarm/alarm.h"
 #include "alarm/queue.h"
/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/

queue alarmqueue;
queue *queuePtr;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/
void init_queue()
{
    queuePtr = &alarmqueue;
    queuePtr->itemCount = 0;
    queuePtr->head = 0;
    queuePtr->tail = -1;
}

void FIFO_enqueue(Alarm *newalarm)
{
    queuePtr->alarms[++queuePtr->tail] = *newalarm;
    queuePtr->itemCount++;
}

Alarm FIFO_dequeue()
{
  Alarm returnAlarm;
    if(isEmpty())
    {
        return;
    }else {
        returnAlarm = queuePtr->alarms[queuePtr->head];
        queuePtr->head++;
        if (queuePtr->head > queuePtr->tail) {
            queuePtr->head = 0;
            queuePtr->tail = -1;
        }
        queuePtr->itemCount--;
    return returnAlarm;
    }
}

int isEmpty() {
   return queuePtr->itemCount == 0;
}



/* ---------- end of module ------------------------------------------------ */

/*@}*/
