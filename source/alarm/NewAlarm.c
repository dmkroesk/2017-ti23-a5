#include "alarm/alarm_internal.h"
#include <stdlib.h>
#include <stdbool.h>
#include "rtc.h"
#include "menu/menu__internal.h"
#include "shoutcast.h"
#include <stdio.h>
#include "display.h"
#include <string.h>

typedef struct
{
    tm alarm_time;
    int stream_index;
    bool time_selected;
    Menu *prev;
}NewAlarm;

__attribute__((always_inline)) static tm tm_change(tm tm, int div)
{
    if (div < 0)
    {
        div *= -1;
        if (tm.tm_min - div >= 0)
        {
            tm.tm_min -= div;
        }
        else
        {
            tm.tm_min = 60 - div;
            if (tm.tm_hour > 0) tm.tm_hour -= 1;
            else tm.tm_hour = 23;
        }
    }
    else if (div > 0)
    {
        if (tm.tm_min + div <= 59) tm.tm_min += div;
        else
        {
            tm.tm_min = 0;
            if (tm.tm_hour < 23) tm.tm_hour += 1;
            else tm.tm_hour = 0;
        }
    }

    return tm;
}

void Alarm_NewAlarm_handle_input(Menu *this, int input)
{
    NewAlarm *na = this->data;
    Alarm *alarm; // C weirdness, since vars can't be declared in a switch
    switch(input)
    {
        case OK:
             alarm = Alarm_Periodic(na->alarm_time, Streams + na->stream_index);
             //alarm->calculate_next_time(alarm);
             queue_enqueue(alarm);
            //printf("alarm has been enqueued\n");
             ALARMCONVERTER_saveAlarm(*alarm);
             Menu_set(na->prev);
            //printf("menu has been set\n");
             na->prev->on_return(na->prev);
            //printf("on return has been called\n");
             break;
        case ESC:
             Menu_set(na->prev);
             na->prev->on_return(na->prev);
             break;
        case UP:
        case DOWN:
            na->time_selected = !na->time_selected;
            break;
        case LEFT:
            if (na->time_selected)
            {
                na->alarm_time = tm_change(na->alarm_time, -5);
            }
            else
            {
                if (na->stream_index) na->stream_index--;
                else na->stream_index = STREAM_COUNT -1;
            }
            break;
        case RIGHT:
            if (na->time_selected)
            {
                na->alarm_time = tm_change(na->alarm_time, 5);
            }
            else
            {
                if (++na->stream_index >= STREAM_COUNT) na->stream_index = 0;
            }
            break;
    }
}

void Alarm_NewAlarm_on_enter(Menu *this, Menu *prev)
{
    NewAlarm *data = this->data;
    tm tm;
    X12RtcGetClock(&tm);
    data->alarm_time.tm_hour = tm.tm_hour;
    data->alarm_time.tm_min = (tm.tm_min / 5) * 5;
    data->stream_index = 0;
    data->time_selected = true;
    data->prev = prev;
    this->data = data;
}

void Alarm_NewAlarm_draw(Menu *this)
{
    NewAlarm *na = this->data;

    char str_buff[17];
    memset(str_buff, ' ', 17);
    SetCursor(0, TOP);
    LcdString(str_buff, 17);
    SetCursor(0, TOP);
    LcdString(str_buff, sprintf(str_buff, " %c%02i:%02i", na->time_selected ? '>' : ' ', na->alarm_time.tm_hour, na->alarm_time.tm_min));

    //@TODO make this a scrolling text
    memset(str_buff, ' ', 17);
    SetCursor(0, BOTTOM);
    LcdString(str_buff, 17);
    SetCursor(0, BOTTOM);
    LcdString(str_buff, sprintf(str_buff, " %c%-14s", na->time_selected ? ' ' : '>', Streams[na->stream_index].name));
}

Menu *Alarm_NewAlarm(void)
{
    Menu *menu = malloc(sizeof(Menu));
    menu->name = "new alarm";

    NewAlarm *data = malloc(sizeof(NewAlarm));
    tm tm;
    X12RtcGetClock(&tm);
    data->alarm_time.tm_hour = tm.tm_hour;
    data->stream_index = 0;
    data->time_selected = true;

    menu->data = data;
    menu->handle_input = Alarm_NewAlarm_handle_input;
    menu->on_enter = Alarm_NewAlarm_on_enter;
    menu->draw = Alarm_NewAlarm_draw;
    return menu;
}

void Alarm_NewAlarmWithTM_on_enter(Menu *this, Menu *prev)
{
    NewAlarm *data = this->data;
    data->prev = prev;
    this->data = data;
}



Menu *Alarm_NewAlarmWithTM(tm tm_param)
{
    Menu *menu = malloc(sizeof(Menu));
    menu->name = "new alarm";

    NewAlarm *data = malloc(sizeof(NewAlarm));
    tm *tmstruct = malloc(sizeof(tm));

    tmstruct->tm_sec = tm_param.tm_sec;
    tmstruct->tm_min = tm_param.tm_min;
    tmstruct->tm_hour = tm_param.tm_hour;
    tmstruct->tm_mday = tm_param.tm_mday;
    tmstruct->tm_mon = tm_param.tm_mon;
    tmstruct->tm_year = tm_param.tm_year;
    tmstruct->tm_wday = tm_param.tm_wday;
    tmstruct->tm_yday = tm_param.tm_yday;

    data->alarm_time = *tmstruct;
    data->stream_index = 0;
    data->time_selected = true;

    menu->data = data;
    menu->handle_input = Alarm_NewAlarm_handle_input;
    menu->on_enter = Alarm_NewAlarmWithTM_on_enter;
    menu->draw = Alarm_NewAlarm_draw;
    return menu;
}
