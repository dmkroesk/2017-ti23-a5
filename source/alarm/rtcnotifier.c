/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     Mainscreen
 * [TITLE]      rtcnotifier header file
 * [FILE]       rtcnotifier.h
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    API and gobal defines for rtcnotifier module
 * ======================================================================== */

#ifndef RTC_NOTIFIER_H
#define RTC_NOTIFIER_H

#include <stdio.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "menu/menu.h"

#include "watchdog.h"
#include "alarm/alarm.h"

#include <time.h>
#include "rtc.h"
#include "alarm/timechecker.h"
#include "alarm/alarm.h"

/*-------------------------------------------------------------------------*/
/* global defines MAGIC NUMBERS                                            */
/*-------------------------------------------------------------------------*/
void create_rtcnotifier_thread(void);
void rtcnotifier_init(void);

/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/

void rtcnotifier_init()
{
    NutThreadSetPriority(2); //what priority?
    create_rtcnotifier_thread();
}

/*!
 * This thread is the rtcnotifierthread thread it asks the alarmchip if a alarm\
 * is passed and checks if a new alarm is set 1x p/s.
 !*/
THREAD(rtcnotifierthread, arg){
  bool musttrigger = 0;
  bool cancheck = 0;
  tm now;
  tm triggertime;

    while(1){
        musttrigger = newalarmflag.flag;
        if(musttrigger)
        {
           printf("rtcnotifier new alarm With trigger time : %i:%i;%i\n", newalarmflag.trigger_time->tm_hour, newalarmflag.trigger_time->tm_min, newalarmflag.trigger_time->tm_sec);
            triggertime = *newalarmflag.trigger_time;
            newalarmflag.flag = 0;
            cancheck = 1;
        }

        if(cancheck){
            X12RtcGetClock(&now);
            if(isbigger_tm(now, triggertime))
            {
                cancheck = 0;
                Alarm_start();
            }
        }

        NutSleep(10000); //sleep. 30 sec minute (1000ms * 30);
      }
}


/*!
 * Creates the keyThread
 */

void create_rtcnotifier_thread()
{
    NutThreadCreate("rtcnotifier", rtcnotifierthread, NULL, 512); //NULL is arguments, 512 is stacksize.
    printf("rtcnotifier thread made \n");
}

#endif /* rtcnotifier */
/*  ����  End Of File  �������� �������������������������������������������� */
