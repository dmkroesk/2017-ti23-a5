#include "alarm/alarm_internal.h"
#include "menu/menu__internal.h"

void Alarm_MenuBase__handle_input(Menu *menu,int input)
{
}

void Alarm_MenuBase__on_enter(Menu *menu, Menu*prev)
{

}

void Alarm_MenuBase__on_return(Menu *menu)
{

}

void Alarm_MenuBase__draw(Menu *menu)
{

}

Menu *Alarm_MenuBase(Alarm *alarm, Menu *deleteMenu, Menu *editMenu)
{
    Menu *menu = malloc(sizeof(Menu));

    menu->name = "temp";

    menu->handle_input = Alarm_MenuBase__handle_input;
    menu->on_enter = Alarm_MenuBase__on_enter;
    menu->on_return = Alarm_MenuBase__on_return;
    menu->draw = Alarm_MenuBase__draw;
}
