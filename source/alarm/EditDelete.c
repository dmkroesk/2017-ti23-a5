#include "alarm/alarm_internal.h"
#include <stdlib.h>
#include <stdbool.h>
#include "rtc.h"
#include "menu/menu__internal.h"
#include "alarm/alarms_menu.h"
#include "shoutcast.h"
#include <stdio.h>
#include "display.h"
#include <string.h>
#include "Alarm/alarm.h"
#include "Alarm/queue.h"

typedef struct
{
    tm tm;
    int index;
    Menu edit;
    Menu *prev;
}EditDelete;

Menu *tempmenu;

void Alarm_EditDelete_handle_input(Menu *this, int input)
{
    EditDelete *editdelete = this->data;
    switch(input)
    {
        case OK:
             if(editdelete->index){
               ALARMCONVERTER_RemoveAlarm(editdelete->tm);
               printf("tijd is %d\n", editdelete->tm.tm_hour);
               queue_remove(editdelete->tm);
               Alarm_EditDelete_on_return(this);
             }
             else {
               tempmenu = Alarm_NewAlarmWithTM(editdelete->tm);
               Menu_set(tempmenu);
               tempmenu->on_enter(tempmenu, this);
             }
             break;
        case ESC:
             Menu_set(editdelete->prev);
             editdelete->prev->on_return(editdelete->prev);
             break;
        case UP:
             editdelete->index = editdelete->index-1;
             if(editdelete->index<0) editdelete->index = 1;
             break;
        case DOWN:
             editdelete->index = editdelete->index+1;
             if(editdelete->index>1) editdelete->index = 0;
             break;
    }
}

void Alarm_EditDelete_on_return(Menu *menu)
{
    EditDelete *editdelete = menu->data;

    main_menu->on_return(main_menu);
    Menu_set(main_menu);

    show_queue();
    queue_remove(editdelete->tm);
    show_queue();
}

void Alarm_EditDelete_on_enter(Menu *this, Menu *prev)
{
  EditDelete *data = this->data;
  data->prev = prev;
}

void Alarm_EditDelete_draw(Menu *this)
{
  EditDelete *editdelete = this->data;
  LcdClear();
  SetCursor(0,TOP);
  LcdString("   Edit Alarm   ",16);
  SetCursor(0,BOTTOM);
  LcdString("  Delete Alarm  ",16);
  if(editdelete->index){
    SetCursor(0,BOTTOM);
    LcdString(">",1);
  }
  else {
    SetCursor(0,TOP);
    LcdString(">",1);
  }
}

Menu *Alarm_EditDelete(tm tm)
{
    Menu *menu = malloc(sizeof(Menu));
    menu->name = "Edit Alarm";

    EditDelete *data = malloc(sizeof(EditDelete));
    data->index = 0;
    data->tm = tm;

    menu->data = data;
    menu->handle_input = Alarm_EditDelete_handle_input;
    menu->on_enter = Alarm_EditDelete_on_enter;
    menu->on_return = Alarm_EditDelete_on_return;
    menu->draw = Alarm_EditDelete_draw;
    return menu;
}
