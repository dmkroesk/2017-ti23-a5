
#define LOG_MODULE  LOG_INET_MODULE

#include <sys/thread.h>
#include <sys/timer.h>

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>


#include "log.h"
#include "shoutcast.h"
#include "vs10xx.h"

#include <sys/socket.h>
#include "player.h"
#include "menu/Stream.h"


#define ETH0_BASE	0xC300
#define ETH0_IRQ	5

#define OK			1
#define NOK			0

const Stream Streams[STREAM_COUNT] = {
		{"Hubu.FM shoutcast", "94.23.53.96", 500},
		{"Radio Prahova Manele", "54.202.122.200", 8000},
		{"HITRADIO CENTER", "94.23.53.96", 500}
};

//Hubu.FM shoutcast
//working streaminfo
int Port = 80;
char *IP1 = "212.18.63.51";

//Hubu.FM shoutcast
//working streaminfo
// #define IP1 ("94.23.53.96")
// #define PORT1 500

//::::..Radio Prahova Manele..:::Non-Stop <----
//working streaminfo
//#define IP1 "54.202.122.200"
//#define PORT1 8000

int Internet = 1;

static char eth0IfName[9] = "eth0";
FILE *stream;

int initInet(void)
{
	printf("%s\n", "releasing previous ip");
	NutDhcpRelease(eth0IfName, 0);
	uint8_t mac_addr[6] = { 0x00, 0x06, 0x98, 0x30, 0x02, 0x76 };

	int result = OK;
	printf("%s\n", "register device");
	// Registreer NIC device (located in nictrl.h)
	if( NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ) )
	{
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutRegisterDevice()"));
		result = NOK;
	}

	printf("%s\n", "DHCP request");
	if( OK == result )
	{
		if(NutDhcpIfConfig(eth0IfName, mac_addr, 30000) )
		{
			LogMsg_P(LOG_ERR, PSTR("Error: >> NutDhcpIfConfig()"));
			result = NOK;

		}
	}

	TCPSOCKET *sock;

	sock = NutTcpCreateSocket();

	if( NutTcpConnect(	sock, inet_addr(IP1),	Port) )
	{
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect()"));

		result = NOK;
	}

	NutTcpCloseSocket(sock);

	if( OK == result )
	{
		LogMsg_P(LOG_INFO, PSTR("Networking setup OK, new settings are:\n") );

		LogMsg_P(LOG_INFO, PSTR("if_name: %s"), confnet.cd_name);
		LogMsg_P(LOG_INFO, PSTR("ip-addr: %s"), inet_ntoa(confnet.cdn_ip_addr) );
		LogMsg_P(LOG_INFO, PSTR("ip-mask: %s"), inet_ntoa(confnet.cdn_ip_mask) );
		LogMsg_P(LOG_INFO, PSTR("gw     : %s\n"), inet_ntoa(confnet.cdn_gateway) );
	}
	else
	{
		Internet = 0;
		LogMsg_P(LOG_ERR, PSTR("initInet() = NOK, NO network!\n"));
		//playBeep();
	}

	printf("%i\n", Internet);
	NutSleep(1000);
	return result;
}

FILE *connect_to_rss(const char *ip, const int port, const char *route)
{
	char      *data;
	TCPSOCKET *socket;
	FILE      *streamrss;

	socket = NutTcpCreateSocket();
	printf("Connecting to ip : %s\nwith Port : %d\nand Route: %s\n",ip,port,route);
	if (NutTcpConnect(socket, inet_addr(ip), 80))
	{
		printf("Error: >> NutTcpConnect() <<");
		return NULL;
	}
	else
	{
		printf("SUCCES: >> NutTcpConnect() <<");

		streamrss = _fdopen((int)socket, "r+b");

		fprintf(streamrss, "GET %s HTTP/1.0\r\n", route); //prepare request
		fprintf(streamrss, "Host: %s\r\n", ip);
		fprintf(streamrss, "User-Agent: Ethernut\r\n");
		fprintf(streamrss, "Connection: close\r\n\r\n");
		fflush(streamrss);

		// Server sends response, catch it
		data = (char *)malloc(512 * sizeof(char));

		char* temp;
		 while((temp = fgets(data, 512, streamrss)) != NULL) //keep going till stream is empty
		 {
		 		printf("%s", temp); //Print response contents
		 };

		printf("\n %s \n", "Done downloading");
		return streamrss;
	}
}


int connectToStream(void)
{
	int result = 1;

	char *data;
	TCPSOCKET *sock;

	if(Internet){
		sock = NutTcpCreateSocket();

		if( NutTcpConnect(sock, inet_addr(IP1),	Port) )
		{
			LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect()"));

			result = 0;
		}
		else{
			stream = _fdopen( (int) sock, "r+b" );

			fprintf(stream, "GET %s HTTP/1.0\r\n", "/");
			fprintf(stream, "Host: %s\r\n", IP1);
			fprintf(stream, "User-Agent: Ethernut\r\n");
			fprintf(stream, "Accept: /\r\n");
			fprintf(stream, "Icy-MetaData: 1\r\n");
			fprintf(stream, "Connection: close\r\n\r\n");
			fflush(stream);

			// Server stuurt nu HTTP header terug, catch in buffer
			data = (char *) malloc(512 * sizeof(char));
			while( fgets(data, 512, stream) )
			{
				if( 0 == *data )
					break;
			}

			free(data);
		}
	}
	else {
		Internet = 0;
	}

	return result;
}

void setipport(char *IP, int port){
	IP1 = IP;
	Port = port;
}

int playStream(void)
{
	play(stream);

	return OK;
}

int stopStream(void)
{
	printf("%s\n", "trying to stop streams");
	fclose(stream);
	stream = NULL;

	return OK;
}

int playBeep()
{
	int i;
	int ii = 1;
	//VsBeepStartRaw(400);

	while(1)
	{
			for(i = 0; i < 6; i++)
			{
				VsPlayerInit();
				VsBeep(((ii*10) * (i + 1)), 1000);
				VsPlayerStop();
			}
			ii = ii +1;
			if(ii == 7)
				break;

			printf("number of itterations is: %d\n",ii );
	}


	return 1;
}
