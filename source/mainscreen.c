/* ========================================================================
 * [PROJECT]    TI23-A5
 * [MODULE]     Mainscreen
 * [TITLE]      mainscreen source file
 * [FILE]       mainscreen.c
 * [VSN]        1.0
 * [COPYRIGHT]  Copyright (C) STREAMIT BV
 * [PURPOSE]    Main menu TODO
 * ======================================================================== */
#define LOG_MODULE  LOG_DISPLAY_MODULE

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <sys/types.h>

#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "../include/system.h"
#include "../include/portio.h"
#include "../include/display.h"
#include "../include/log.h"
#include "../include/rtc.h"
#include "../include/mainscreen.h"

/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/
/*!
 * Draws the entire mainscreen.
 * Displays time, internetconnection, alarm status
 * and volume status.
 */
void DrawMainscreen(){


}



/* ---------- end of module ------------------------------------------------ */

/*@}*/
