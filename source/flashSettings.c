
//#include <sys/thread.h>
#include <sys/timer.h>
#include "flash.h"
#include "flashSettings.h"
#include "userdata.h"
#include <string.h>
#include <menu/Volume.h>
#include <menu/FirstStartup.h>
#define NOK -1

static USERDATA_STRUCT userStruct[]= {{}};

// saves the struct to the selected page
static int savePersistent(USERDATA_STRUCT *src, int size, int page)
{
	int result = NOK;
	unsigned char *storage = (unsigned char *) malloc(sizeof(unsigned char) * size);
	if( storage != NULL )
	{
		memcpy( (unsigned char *)storage, src, size );
		At45dbPageWrite(page, (unsigned char *)storage, size);

		result = 0;
	}
	free(storage);
	return result;
}

// opens the selected page and memcopies it to the existing struct
static int openPersistent(USERDATA_STRUCT *src, int size, int page)
{
	int result = NOK;

	unsigned char *storage = (unsigned char *) malloc(sizeof(unsigned char) * size);
	if( storage != NULL )
	{
		At45dbPageRead(page, (unsigned char *)storage, size);
		memcpy( (USERDATA_STRUCT *) src, (unsigned char *)storage, size );

		result = 0;
	}
	free(storage);
	return result;
}

Volume* volumeStruct;

void FLASHSETTINGS_INIT(Volume* volume)
{
	volumeStruct = volume;
}


// function that saves all the settings to the dedicated page.
void FLASHSETTINGS_SAVE_SETTINGS()
{
	// when saving the actual data is being pulled.
	userStruct[0].pageID = 1337;
	//this is a page id which indicates the kind of the page
	userStruct[0].volume = volumeStruct->volume;
	// the volume value which is de actual volume
	userStruct[0].GMTSetting= GMT_TIME;
	// the GMT value is the value that has been set in the beginning
	userStruct[0].setting2 = 4;
	// setting 2 is not being used but is here for expansion purposes
	printf("saving to flash with volume: %d", volumeStruct->volume);
	printf("saving to flash with GMT: %d", GMT_TIME);
	//debug prints


	if(0 == savePersistent((USERDATA_STRUCT *)userStruct, sizeof(userStruct[0]), 0x05) )
	{
		printf("\nsaved settings\n");
		NutSleep(150);
		//nut sleep to give the program some time to save.
	}
	else
		printf("Error saving persistent data");
}

//opens the dedicated settings page
void FLASHSETTINGS_OPEN_SETTINGSPAGE(void)
{
		openPersistent( (USERDATA_STRUCT *)userStruct, sizeof(userStruct[0]), 0x05 );
		showUserData((USERDATA_STRUCT *)userStruct, sizeof(userStruct)/ sizeof(userStruct[0]));
		FLASHSETTINGS_SET_SETTINGS();
}

 // saves the values from Open-setting-page to the variables in the program.
 void FLASHSETTINGS_SET_SETTINGS(void)
 {
	 volumeStruct->volume = userStruct->volume;
	 GMT_TIME = userStruct->GMTSetting;
 }

// returns 1 when first startup
// 1337 is settings pageID
int FLASHSETTINGS_STARTUP_CHECK()
{
	FLASHSETTINGS_OPEN_SETTINGSPAGE();
	if(userStruct[0].pageID == 1337)
	{
		printf("not first startup\n");
		//showUserData((USERDATA_STRUCT *)userStruct, sizeof(userStruct)/ sizeof(userStruct[0]));

		return 0;
	}
	else
	{
		printf("first startup\n");
			return 1;
	}
}
// returns 1 when reset successfully
int FLASHSETTINGS_RESETFLASH()
{
	userStruct[0].pageID = 00;
	userStruct[0].volume = 125;
	userStruct[0].GMTSetting = 00;
	userStruct[0].setting2 = 00;

	int saveResult =  savePersistent((USERDATA_STRUCT *)userStruct, sizeof(userStruct[0]), 0x05);
	printf("result is: %d\n", saveResult );
	FLASHSETTINGS_OPEN_SETTINGSPAGE();
	if(userStruct[0].pageID == 00)
	{
		printf("Reset successful\n");
		return 1;
	}
	else
	{
		printf("reset not succesful\n" );
		return 0;
	}
}

//prints the userdata struct mainly for debuging
void showUserData(USERDATA_STRUCT *userStruct, int len)
{
	int idx = 0 ;
	for( idx = 0; idx < len; idx++ )
	{
    printf("streams[%d].pageID = %i\n\n", idx, userStruct[idx].pageID);
		printf("streams[%d].volume = %d\n\n", idx, userStruct[idx].volume);
    printf("streams[%d].setting1 = %d\n\n", idx, userStruct[idx].GMTSetting);
    printf("streams[%d].setting2 = %d\n\n\n", idx, userStruct[idx].setting2);
	}
}
